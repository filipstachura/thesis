\chapter{Implementacja platformy do eksperymentów}
\label{chap:architektura}

\section{Wprowadzenie}
W tym rozdziale została opisana architektura i implementacja systemu, na którym dokonano eksperymentów praktycznych zaproponowanego w poprzednim rozdziale algorytmu. Przedstawiono również elementy, które były użyte w procesie zbierania danych do eksperymentów.

Tworzenie systemów działających w czasie rzeczywistym jest niezwykle rozległym tematem. Szczegółowe informację na temat budowy i działania takich systemów, protokołów komunikacji i typowych wzorców projektowych można znaleźć w książce Byrona Ellisa \cite{ellis}. W przypadku prawdziwych wdrożeń systemów informatycznych działających w czasie rzeczywistym niezbędne jest na przykład zapewnienie działania systemu nawet w przypadku kiedy potencjalne zależności przestają działać. W przypadku badań teoretycznych niepotrzebne jest rozpatrywanie przypadków niepożądanych. Niezbędne jest jednak zebranie prawdziwych danych, na których można przeprowadzić badania.

\section{Opis architektury}

Podstawową cechą architektury stworzonego systemu jest jej rozproszony charakter. Różnego rodzaju urządzenia i programy, korzystając z określonego protokołu, mają możliwość komunikacji ze sobą. W szczególności w zaimplementowanej architekturze jest możliwe stworzenie zespołu sensorów raportujących wartości odczytów do systemu, a także klasyfikatora, który na bazie dostarczanych odczytów w czasie dokonuje predykcji.

Elementem centralnym systemu jest aplikacja \emph{dyspozytora} (ang. \emph{dispatcher}). \emph{Dyspozytor} odpowiada za komunikację wszystkich pozostałych elementów systemu.
Pozwala on na komunikację w dwie strony, to znaczy że dowolny element może za jego pomocą wysyłać wiadomości do innych i je odbierać. Niektóre elementy systemu mają za zadanie jedynie nadawanie, inne tylko odbierają wiadomości, najbardziej rozbudowane - zarówno odbierają jak i wysyłają wiadomości.

\begin{figure}[!htb]
  \centering
    \input{tikz/architektura.tex}
  \caption{Diagram komunikacji z \emph{dyspozytorem} w ramach architektury zaimplementowanego systemu.}
  \label{fig:diagarch}
\end{figure}

Na diagramie~\ref{fig:diagarch} przedstawiono przykład komunikacji elementów systemu z \emph{dyspozytorem}. Można na nim zaobserwować, że zbiór sensorów jedynie wysyła informację, a interfejs użytkownika dostępny przez stronę WWW jedynie nasłuchuje informacji aby je zaprezentować.

\subsection{Kod źródłowy \emph{dyspozytora}}

Pomimo bardzo istotnej roli jaką pełni \emph{dyspozytor}, sama jego implementacja jest prosta. Program został napisany w języku \emph{JavaScript} i działa na platformie \emph{Node.js}. Platforma \emph{Node.js} zyskała w ostatnich latach szeroką popularność pozwalając na uruchamianie kodu \emph{JavaScript} po stronie serwera. Technologia ta jest wykorzystywana bardzo często przy zagadnieniach czasu rzeczywistego.

Do komunikacji wykorzystano protokół \emph{WebSocket}, zapewniający dwustronną komunikację za pośrednictwem jednego gniazda TCP. Z tego protokołu system korzysta za pośrednictwem biblioteki \emph{Socket.IO}.

\begin{listing}[H]
\begin{minted}[mathescape,
               linenos,
               numbersep=5pt,
               gobble=2,
               frame=lines,
               framesep=2mm]{js}
  var io = socketio.listen(app.listener);

  function readSignal(data) {
    io.emit(data.name, data);
    io.emit('*', data);
  }

  function registerHandlers(socket) {
    socket.on('readSignal', readSignal);
  }

  io.sockets.on('connection', registerHandlers);
  app.start();
\end{minted}
\caption{Kod źródłowy \emph{dyspozytora}.}
\label{lst:codedispatcher}
\end{listing}

Kluczowy fragment kodu źródłowego został umieszczony na listingu~\ref{lst:codedispatcher}. Program akceptuje wszystkie połączenia, a później otrzymane wiadomości o właściwym nagłówku przekazuje dalej do wszystkich oczekujących połączeń.

\subsection{Przykład komunikacji z \emph{dyspozytorem}}

\begin{listing}[H]
\begin{minted}[mathescape,
               linenos,
               numbersep=5pt,
               gobble=2,
               frame=lines,
               framesep=2mm]{js}
  import client = require("socket.io-client");
  var socket = client.connect(dispatcher);
  var oneSecond = 1000;

  var range = {
    start: 0,
    end: 100
  };

  function reportRandom() {
    var dist = range.end - range.start;
    var value = Math.round(Math.random() * dist) + range.start;
    socket.emit("readSignal", {name: "random", value: value});
  };

  var interval = setInterval(reportRandom, oneSecond);
\end{minted}
\caption{Kod źródłowy: przykładowy sensor.}
\label{lst:coderandom}
\end{listing}

\begin{listing}[H]
\begin{minted}[mathescape,
               linenos,
               numbersep=5pt,
               gobble=2,
               frame=lines,
               framesep=2mm]{js}
  import client = require("socket.io-client");
  import tracer = require('tracer');

  var logger = tracer.console({
    format: "{{timestamp}} {{message}}",
    dateformat: "HH:MM:ss",
  });

  var socket = client.connect(dispatcher);

  socket.on('connect', function () {
    console.log("Connected with host: ", dispatcher);
  });

  socket.on('*', function(signal) {
    logger.log('[%s] %s', signal.name, JSON.stringify(signal.value));
  });
\end{minted}
\caption{Kod źródłowy: odbieranie wiadomości od \emph{dyspozytora}}
\label{lst:codelogger}
\end{listing}

Na listingu~\ref{lst:coderandom} został przedstawiony prosty przykład sensora. W tym wypadku do \emph{dyspozytora} co sekundę wysyłane są losowe odczyty. Przypadek, w którym raportowane są bardziej skomplikowane dane wygląda bardzo podobnie.

Dla porównania program, który odbiera wiadomości od \emph{dyspozytora} jest zapisany w listingu~\ref{lst:codelogger}. Ma on za zadanie wypisywanie na standardowe wyjście wszystkich wiadomości wraz z informacją o czasie otrzymania.

Wszystkie umieszczone programy zostały stworzone w języku \emph{JavaScript}, ale w celu komunikacji z dyspozytorem możliwe jest użycie dowolnej platformy umożliwiającej komunikację przez \emph{Socket.IO}. W szczególności możliwa jest implementacja sensorów lub klasyfikatorów w językach kompilowanych do \emph{JavaScript} takich jak \emph{TypeScript} lub \emph{CoffeeScript}. Istnieją również projekty mające na celu zapewnienie możliwości komunikacji poprzez \emph{Socket.IO} dla takich języków jak \emph{Java} czy \emph{C++} i wielu innych \cite{socketio}.

\section{Wykorzystanie frameworku \emph{MOA}}

\emph{MOA} (\emph{Massive online analysis}) jest platformą programistyczną udostępnianą jako wolne oprogramowanie (ang. \emph{open source}) na licencji \emph{GNU GPL}, która pozwala przeprowadzać eksperymenty na strumieniach danych \cite{wwwmoa}. \emph{MOA} jest ściśle związana z projektem \emph{WEKA}, popularną platformą zajmującą się uczeniem maszynowym \cite{wwwweka}. Oba projekty są rozwijane na Uniwersytecie Waikato (Nowa Zelandia).

\emph{MOA} zawiera implementację klasyfikatorów i generatorów strumieni, które mogą być używane zarówno poprzez interfejs graficzny, terminal jak i przez API w języku \emph{Java}. Algorytmy zaprojektowane w ramach platformy mają działać na strumieniach danych w czasie rzeczywistym i radzić sobie z kwestią zmiany rozkładu danych w czasie.

W przypadku przedstawionych badań zostało wykorzystane API udostępniane przez platformę programistyczną. Wykorzystana została implementacja drzew Hoeffdinga, będąca częścią platformy.

Predyktory komunikujące się z \emph{dyspozytorem} zostały zaimplementowane w języku \emph{TypeScript}. Możliwość komunikacji z API dla języka \emph{Java} z poziomu platformy \emph{Node.js} została zapewniona przez bibliotekę o nazwie \emph{java} \cite{npmjava}.

\section{Oprogramowanie urządzeń mobilnych}

W obu przeprowadzonych eksperymentach praktycznych zaszła konieczność implementacji aplikacji mobilnej. Za każdym razem aplikacje komunikowały się z \emph{dyspozytorem} i wysyłały informacje dotyczące odczytów z sensorów.

Do implementacji aplikacji została wykorzystana platforma \emph{Apache Cordova}.  \emph{Apache Cordova} jest platformą programistyczną pozwalającą tworzyć aplikacje na wiodące mobilne systemy operacyjne wykorzystując \emph{HTML}, \emph{CSS} i \emph{Javascript}. Platforma jest udostępniana jako wolne oprogramowanie (ang. \emph{open source}) na licencji \emph{Apache 2.0}.

Aplikacje stworzone przy użyciu tej platformy mają dostęp do rozwiązań natywnych telefonu poprzez rozszerzenie tradycyjnych możliwości stron internetowych. Komunikacja poprzez \emph{Socket.IO} jest możliwa poprzez specjalny plugin do platformy \cite{cordovasocketio}.

Wygląd stworzonej aplikacji na rzecz pierwszego z eksperymentów, opisanego w następnym rozdziale został zamieszczony na rysunku~\ref{fig:cordova}.
\begin{figure}[!htb]
\centering
  \includegraphics[scale=0.25]{img/mobile.png}
\caption{Wygląd aplikacji stworzonej przy użyciu platformy programistycznej \emph{Apache Cordova}. Aplikacja została użyta do komunikacji z \emph{dyspozytorem} w trakcie przeprowadzonych eksperymentów praktycznych.}
\label{fig:cordova}
\end{figure}
