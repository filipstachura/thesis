\chapter{Strumienie danych}

W poniższym rozdziale omówione zostały strumienie danych. Generują one nowe instancje danych z dużą częstotliwością, zazwyczaj w sposób automatyczny.
Konkretnymi przykładami środowisk, w których są obecne strumienie danych są:
\begin{itemize}
  \item sieci sensorów,
  \item dane telekomunikacyjne,
  \item rynki finansowe,
  \item sieci internetowe,
  \item web mining.
\end{itemize}

\section{Założenia i wymagania}
\label{wymagania}
W zagadnieniu strumieniowym rozważane dane często są bardzo dużych rozmiarów, znacznie przewyższających rozmiar dostępnej pamięci komputera.
Z tego powodu rezygnuje się z zapisu tych danych, przyjmując założenie o ich ulotnym charakterze. Oznacza to, że w pamięci komputera dostęp jest możliwy jedynie do stałej liczby instancji. W celu stworzenia efektywnych algorytmów, biorących pod uwagę te nowe założenie, wielu autorów wymienia wymagania, które algorytm tego typu powinien spełniać \cite{gama2}, \cite{moa}, \cite{ullman}. Najważniejsze wymagania dotyczące nowego modelu obliczeniowego to:
\begin{enumerate}
  \item \textbf{Dostępność danych jest ograniczona.} \\
    Jedną z kluczowych cech strumieni danych jest zgłaszanie się kolejnych obserwacji w czasie. Algorytm sam nie może w żaden sposób uzyskać
    dostępu do przyszłych danych, a także do tych przeszłych danych, które nie zostały zapamiętane. Należy zakładać, że algorytm nie ma
    żadnego wpływu na kolejność przetwarzanych danych. Dodatkowo dostępne dane zazwyczaj nie są wzajemnie niezależne, jak jest często
    zakładane przy metodach klasycznych. Istnieje również możliwość, że rozkład danych zmienia się w czasie.
    Po przetworzeniu obserwacji algorytm musi ją bezpowrotnie odrzucić, bez możliwości ponownej jej analizy
    (co prawda niewielką, skończoną liczbę może zapamiętać, ale prawie wszystkie obserwacje zostaną bezpowrotnie odrzucone).
  \item \textbf{Obowiązują ograniczone zasoby obliczeniowe.} \\
    W pierwszej kolejności ograniczenie dotyczy dostępnej pamięci i czasu przetwarzania, ale także
    poboru mocy i komunikacji. Czas potrzebny na analizę jednej instancji powinien być stały i istotnie krótki. \\
    Pierwsze z ograniczeń wynika wprost, z założenia o tym, że rozmiar danych strumieniowych może przekraczać rozmiar dostępnej pamięci.
    Ograniczenie czasu przetwarzania ma na celu zapewnienie płynnego działania algorytmu.
    Obrazuje to hipotetyczny przypadek w którym czas potrzebny na przetworzenie jednej instancji zależy od rozmiaru danych.
    Przypuśćmy, że algorytm przetwarza jedną obserwację w złożoności $O(log(n))$, gdzie $n$ wzrasta wraz z czasem działania algorytmu.
    Na początku funkcjonowania systemu może nie być to żadnym problemem,
    ale wraz z napływem nowych obserwacji (potencjalnie nieograniczonej ich liczbie) system ulegnie zablokowaniu. Kolejne zgłoszenia będą napływać
    do systemu, ale ponieważ czas przetwarzania ciągle będzie wzrastał, to przepustowość w pewnym momencie zostanie przekroczona.
  \item \textbf{Tworzone rozwiązania powinny być dostępne i gotowe do użycia w dowolnym momencie.} \\
    W najlepszym wypadku algorytm powinien w danej sytuacji udostępniać najlepiej dopasowany model do dotychczas zaobserwowanych danych.
    Warto podkreślić, że sytuacją preferowaną jest, aby model predykcyjny tworzony był możliwie wydajnie, bez konieczności ponownego jego
    generowania na podstawie utrzymywanych statystyk.
\end{enumerate}

\section{Przykłady badanych zagadnień}
W środowisku strumieni danych można rozważać rozmaite problemy dobrze zbadane w klasycznych przypadkach, jak na przykład klasteryzacja czy detekcja obserwacji odstających. Pojawiają się również nowe zadania, które dla danych statycznych nie zachodziły.

Przykłady zagadnień, które można rozważać dla strumieni danych:
\begin{enumerate}
  \item klasyfikacja, klasteryzacja, detekcja obserwacji odstających,
  \item problem popularnego podzbioru,
  \item utrzymanie statystyk dla strumienia danych - zagadnienie proste w przypadku klasycznym, może okazać się nieoczywiste w przypadku strumieniowym,
  \item streszczanie danych - uzyskanie wiarygodnych estymacji na temat rozkładu danych w całym strumieniu.
\end{enumerate}

W szczególności badane problemy powinny dać się zapisać w formie zagadnienia adaptacyjnego uczenia, zgodnie z definicją \ref{adapucz}.

\begin{defi}[Adaptacyjne zagadnienie uczenia]
\label{adapucz}
  Niech $T_t=\{(\vec{x_i}, y_i)_{i=1,\dots,t}: f(\vec{x_i})=y_i\}$, będzie zbiorem danych dostępnych w momencie $t$. Algorytm uczący nazywa się adaptacyjnym jeżeli dla sekwencji zbiorów $\{\dots,T_{j-1},T_j,T_{j+1},\dots\}$ tworzy on sekwencję hipotez $\{\dots,H_{j-1},H_j,H_{j+1},\dots\}$, gdzie każda hipoteza $H_j$, zależy wyłącznie od hipotezy poprzedniej $H_{j-1}$ oraz aktualnej obserwacji $(\vec{x_j},y_j)$.
\end{defi}

W kolejnych sekcjach zostały omówione podstawowe metody operowania na strumieniach: utrzymanie statystyk, streszczanie danych i zagadnienie klasyfikacji. Warto zauważyć, że każda z tych metod dobrze wpisuje się w definicję adaptacyjnego uczenia.

\subsection{Utrzymanie statystyk}
\label{sec:stats}

Wybrane statystki przydają się przy wielu operacjach mających związek z analizą danych strumieniowych. Warto rozważyć, które z nich mogą być utrzymywane niewielkim, stałym kosztem. Pod koniec został również opisany przykład statystyki dla której nie istnieje wydajna metoda.

\subsubsection{Średnia}
Rekurencyjna wersja równania na średnią z próby przedstawia się w następujący sposób:
$$ \hat{x_t} = \frac{(t-1) * \hat{x_{t-i}} + x_t}{t} $$

W praktyce, aby uniknąć błędów wynikających z zaokrągleń bezpieczniej jest aktualizować wartość sumy $\sum_0^t{x_i}$ oraz liczby przetworzonych dotychczas obserwacji $t$. Na podstawie tych dwóch statystyk, średnia może być łatwo wyliczona jako ich iloraz.

\subsubsection{Wariancja}
W analogiczny sposób może zostać wyliczana wariancja dla danych strumienia:
$$ s_t^2 = \frac{\sum_0^t{(x_i - \hat{x_t})^2}}{t} = \frac{\sum_0^t{x_i^2} - 2\hat{x_t}\sum_0^t{x_i} + t\hat{x_t}^2}{t} = \frac{\sum_0^t{x_i^2}}{t} - \hat{x_t}^2$$

Aby wyliczyć wariancję strumienia danych, wystarczy aktualizować stan następujących zmiennych:
\begin{itemize}
  \item Sumy obserwacji $\sum{x_i}$
  \item Sumy kwadratów obserwacji $\sum{x_i^2}$
  \item Licznika łącznej liczby dotychczasowych obserwacji $t$
\end{itemize}

\subsubsection{Współczynnik korelacji}
W przypadku danych z dwóch strumieni istotne może okazać się utrzymywanie stanu wskaźnika ich wzajemnej korelacji.
Mając dane strumienie $x$ i $y$ można wyliczyć korelację w następujący sposób:
$$ corr(x_t, y_t) = \frac
                      {\sum_0^t{(x_i-\hat{x_t})(y_i-\hat{y_t})}}
                      {\sqrt{\sum_0^t{(x_i-\hat{x_t})^2}} {\sqrt{\sum_0^t{(y_i-\hat{y_t})^2}}}}$$

$$ corr(x_t, y_t) = \frac
                      {\sum_0^t{x_iy_i}-2n\hat{x_t}\hat{y_y}+n\hat{x_t}\hat{y_t}}
                      {\sqrt{\sum_0^t{x_i^2}-2\hat{x_t}\sum_0^t{x_i}+n\hat{x_t}^2} \sqrt{\sum_0^t{y_i^2}-2\hat{y_t}\sum_0^t{y_i}+n\hat{y_t}^2}}$$

$$ corr(x_t, y_t) = \frac
                      {\sum_0^t{x_iy_i} - n\hat{x_t}\hat{y_t}}
                      {\sqrt{\sum_0^t{x_i^2} - n\hat{x_t}^2} \sqrt{\sum_0^t{y_i^2}-n\hat{y_t}^2}} $$

W tym wypadku poza statystykami potrzebnymi do obliczenia wariancji zmiennych $x$ i $y$, należy również utrzymywać wartość iloczynu wektorowego $x$ i $y$ tj. $\sum{x_iy_i}$

\subsubsection{Metoda przesuwanego okna}

Często od wartości statystyki dla wszystkich przetworzonych obserwacji bardziej istotne okazują się dane lokalne, bliskie teraźniejszości.
Najprostszym przykładem struktury pozwalającej operować na tych danych jest okno czasowe określonego rozmiaru, przesuwające się wraz z danymi w czasie.
Najczęściej w literaturze spotyka się dwa rodzaje przesuwanych okien:
\begin{itemize}
  \item \textbf{Określonego rozmiaru} (ang. \emph{Sequence based}) -  W tym wypadku rozmiar okna jest stały i skończony. Struktura przypomina kolejkę FIFO. Za każdym razem kiedy algorytm przetwarza nową obserwację, ostatnia (najstarsza obserwacja) zostaje usunięta.
  \item \textbf{Określonego zakresu czasowego} (ang. \emph{Timestamp based}) -  Wielkość okna jest określona na podstawie zakresu czasowego. Okno o rozmiarze $t$ zawiera wszystkie obserwacje odnotowane w przedziale czasowym o tym rozmiarze do chwili obecnej. Algorytm powinien być dodatkowo przygotowany na możliwość potencjalnego przekroczenia ograniczenia pamięci i obsłużyć taki wypadek w odpowiedni sposób.
\end{itemize}

Wyliczanie statystyk dla okna czasowego wymaga przetrzymywania w pamięci wszystkich elementów okna. Odpowiednie sumy i wskaźniki z poprzedniej
sekcji muszą być odpowiednio aktualizowane za każdym razem gdy dowolny element jest usuwany lub dodawany do okna.

Prostym przykładem ukazującym problemy z utrzymaniem prostych statystyk dla strumieni
danych jest zagadnienie wartości maksymalnej dla przesuwanego okna. Oczywiście
jeśli zasoby pozwalają na przetrzymywanie pełnego okna w pamięci zagadnienie jest
proste. W przypadku gdy rozmiar okna przekracza dostępne zasoby algorytm znajdujący
dokładne rozwiązanie nie istnieje.

Jako ilustrację tego problemu można rozważyć monotoniczny malejący strumień, w którym każda kolejna instancja jest mniejsza o losową wartość.
Zakładając, że rozmiar okna wynosi $k$ i obserwacja $x_i$ nie zostaje zapamiętana z powodu ograniczenia pamięciowego, w chwili $k'=i+k$ wartość $x_i$ jest elementem maksymalnym dla przesuwanego okna, co wynika z monotoniczności strumienia. Ponieważ wartość jest losowa i nie została zapisana w pamięci algorytm nie może odtworzyć prawidłowej wartości statystyki.

\subsection{Streszczenie danych}

Alternatywą dla metody przesuwanego okna jest uzyskanie jedynie aproksymacji pożądanych statystyk, ale na szerszym zakresie danych.
W celu uzyskania aproksymacji należy zredukować rozmiar danych, zachowując możliwie maksymalną ilość właściwości danych. Dwie najbardziej popularne metody, to:

\subsubsection{Próba}
Selekcja próby jest popularna metodą reprezentacji większego zbioru danych. Dzięki temu, zamiast analizować wszystkie elementy zbioru, algorytm może ograniczyć się jedynie do grupy reprezentantów. Estymacje statystyk mogą być następnie utworzone bazując na grupie reprezentantów. Najważniejszym zagadnieniem jest wyselekcjonowanie obserwacji, które faktycznie będą dobrze reprezentowały całościowe dane.

Większość metod statystycznych dokonujących selekcji próby zakłada znajomość całościowego rozmiaru danych. Metodą nadającą się do zastosowania w przypadku strumieni danych jest technika \emph{próby zbiornika} (ang. \emph{reservoir sampling}) opisana przez Vittera \cite{vitter}. W tej metodzie zakłada się stały rozmiar próby, nazywanej \emph{zbiornikiem}. Zakładając, że rozmiar zbiornika wynosi $k$. W miarę przepływu strumienia danych nowy, $n$-ty element z prawdopodobieństwem $k/n$ może zastąpić dowolny element obecnie przechowywany w zbiorniku. Dzięki temu podejściu, po rozważaniu $n$-tego elementu każdy z dotychczasowych elementów może znajdować się w zbiorniku z równym prawdopodobieństwem.

Interesującym innym podejściem jest metoda \emph{Min-Wise sampling}. W tej metodzie dla każdego elementu strumienia losowana jest liczba z przedziału $[0,1]$. W próbie zostają zachowane elementy o najmniejszych wylosowanych wartościach. Z zastosowanej metody wprost wynika, że każdy element może znaleźć się w próbie z równym prawdopodobieństwem. Dużą zaletą tej metody jest możliwość łatwego zastosowania jej w środowisku rozproszonym, gdzie wystarczy zachować elementy o najmniejszych wartościach z sumy zbiorów.

Inne metody wyboru próby, oparte o haszowanie, opisał Ullman et al. \cite{ullman}.

\subsubsection{Histogram}

Użycie histogramu jest popularnym sposobem na uzyskanie aproksymacji rozkładu danych. Histogram jest zdefiniowany przez zbiór rozłącznych przedziałów, o sumie zawierającej wszystkie możliwe wartości atrybutu. Każdy przedział opisany jest przez jego granice oraz licznik elementów. Dla danych jakościowych jest oczywiście możliwe stworzenie histogramu na strumieniu danych. W przypadku danych numerycznych jest to zadanie niebanalne, ponieważ zbiór raz ustalonych przedziałów jest bardzo trudno modyfikować w trakcie działania algorytmu. Istnieją metody aproksymacji histogramu dla danych napływających w czasie, w celu wyznaczania odpowiednich przedziałów \cite{gama2}.

\subsection{Zagadnienie klasyfikacji}
Definicję zagadnienia klasyfikacji dla uczenia maszynowego w sposób naturalny można przenieść na przypadek adaptacyjny:

\begin{defi}[Strumieniowe zagadnienie klasyfikacji]
  Niech: $$T_t=\{(\vec{x_i}, y_i)_{i=1,\dots,t}: f(\vec{x_i})=y_i\}$$ będzie zbiorem danych dostępnych w momencie $t$, gdzie $\forall_{i}\vec{x_i} \in \mathcal{X}$ oraz $\forall_{i}y_i \in \mathcal{Y}$. Strumieniowe zagadnie klasyfikacji polega na utworzeniu sekwencji hipotez $\{\dots,H_{j-1},H_j,H_{j+1},\dots\}$, na podstawie sekwencji zbiorów $\{\dots,T_{j-1},T_j,T_{j+1},\dots\}$ , gdzie każda hipoteza $H_j$, zależy wyłącznie od hipotezy poprzedniej $H_{j-1}$ oraz obserwacji $(\vec{x_j},y_j)$. Dodatkowo każda z hipotez $H_j$ musi jednoznacznie wyznaczać klasyfikator $C_j: \mathcal{X}\rightarrow\mathcal{Y}$, który przydziela obiektowi $\vec{x} \in \mathcal{X}$ klasę $y \in \mathcal{Y}$.
\end{defi}

Dodatkowo w zastosowaniach praktycznych najczęściej spotykane są poniższe wymagania:
\begin{enumerate}
  \item Liczba atrybutów jest niewielka (tj. wymiar $\mathcal{X}$ jest niewielki).
  \item Rozmiar danych treningowych jest potencjalnie nieskończony i zdecydowanie większy od dostępnej pamięci.
  \item Dostępny jest stały czas na klasyfikację lub predykcję pojedynczego obiektu, aby umożliwić liniowe skalowanie algorytmu.
  \item Liczba klas jest względnie niewielka (tj. moc $\mathcal{Y}$ jest niewielka).
\end{enumerate}

\section{Naiwny klasyfikator bayesowski}

Naiwny klasyfikator bayesowski, opisany w poprzednim rozdziale, w sposób naturalny spełnia wymagania stawiane przed adaptacyjnym klasyfikatorem. Do aktualizacji modelu klasyfikatora faktycznie potrzebny jest jedynie model stworzony przy użyciu obserwacji poprzedniej, oraz obecna instancja. Na podstawie modelu tworzony jest zaś klasyfikator i może być on utworzony w dowolnym momencie działania algorytmu.

Można rozważyć przykładowy przebieg działania algorytmu, dla danych:
\begin{itemize}
  \item o trzech atrybutach $X_1=\{A,B,C\}, X_2=\{D,E\}, X_3=\{F,G,H\}$,
  \item dwóch klasach: $C_1, C_2$
\end{itemize}

Przykładowo, algorytm po 200 obserwacjach mógł stworzyć następującą hipotezę $H_{200}$:

\begin{center}
  \begin{tabular}{ | l || l | l | l || l | l || l |l |l ||l |l |}
    \hline
          & \multicolumn{3}{c||}{$X_1$} & \multicolumn{2}{c||}{$X_2$} & \multicolumn{3}{c||}{$X_3$} & \\ \hline
    Klasa & $A$ & $B$ & $C$           & $D$  & $E$                & $F$ & $G$ & $H$           & Razem \\ \hline
    $C_1$ & 40  & 20  & 60            & 70   & 50                 & 55  & 15  & 50            & 120 \\ \hline
    $C_2$ & 40  & 30  & 10            & 30   & 50                 & 20  & 30  & 30            & 80  \\ \hline
  \end{tabular}
\end{center}

Następnie do algorytmu przekazana została obserwacja $(\vec{x_{201}}, y_{201})=((B,E,F), C_1)$. Aktualizacja modelu (hipotezy), algorytmu nie wymaga dodatkowej pamięci i potrzebuje jedynie stałej złożoności czasowej, w jej wyniku otrzymuje się hipotezę $H_{201}: $
\begin{center}
  \begin{tabular}{ | l || l | l | l || l | l || l |l |l ||l |l |}
    \hline
          & \multicolumn{3}{c||}{$X_1$} & \multicolumn{2}{c||}{$X_2$} & \multicolumn{3}{c||}{$X_3$} & \\ \hline
    Klasa & $A$ & $B$ & $C$           & $D$  & $E$                & $F$ & $G$ & $H$           & Razem \\ \hline
    $C_1$ & 40  & \textbf{21}  & 60            & 70   & \textbf{51}                 & \textbf{56}  & 15  & 50            & \textbf{121} \\ \hline
    $C_2$ & 40  & 30  & 10            & 30   & 50                 & 20  & 30  & 30            & 80  \\ \hline
  \end{tabular}
\end{center}

Na podstawie dowolnej z hipotez $H_j$, może zostać utworzony klasyfikator bayesowski. Przykładowo dla hipotezy $H_{200}$ chcąc dokonać klasyfikacji obserwacji $\vec{x}=(A,E,H)$ klasyfikator wykona następujące działania:
\begin{enumerate}
  \item Obliczy dla $i=1,2$:
    $$p_i = P(C=C_i | X=(A,E,H)) = \frac{P(X=(A,E,H)|C=C_i)P(C=C_i)}{P(X=(A,E,H))} $$
    $$p_i \propto P(X=(A,E,H)|C=C_1)P(C=C_i)$$
  \item Otrzymując:
    $$p_1 \propto \frac{40}{120}\frac{50}{120}\frac{50}{120}\frac{120}{200} \approx 0.035 $$
    $$p_2 \propto \frac{40}{80}\frac{50}{80}\frac{30}{80}\frac{80}{200} \approx 0.047 $$
  \item Na podstawie tych wyników klasyfikując obserwację jako klasę $C_2$
\end{enumerate}

Należy podkreślić, że przedstawiony algorytm spełnia wszystkie wymagania stawiane algorytmom strumieniowym:
\begin{itemize}
  \item dostęp do danych jest ograniczony - do aktualizacji modelu potrzebna jest jedynie ostatnia obserwacja i wcześniej stworzony model,
  \item wykorzystywane są jedynie stałe, niewielkie zasoby pamięciowe, zarówno czas aktualizacji modelu i klasyfikacji jest stały względem rozmiaru obserwacji,
  \item dostęp do klasyfikatora może nastąpić w dowolnym momencie.
\end{itemize}


\section{Metody ewaluacji jakości modelu}

Klasyczne metody ewaluacji nie mogą być bezpośrednio użyte do oceny klasyfikatorów strumieniowych. Podobnie jak w przypadku klasycznym najbardziej skutecznym klasyfikatorem będzie ten, który popełnia najmniej błędów dokonując predykcji na wszystkich danych. Tutaj również wszystkie dane nie są dostępne i należy posłużyć się estymacjami. Istotne jest więc określenie wiarygodnych metod wyznaczania estymacji skuteczności klasyfikatorów.

W kontekście danych strumieniowych niewielka ilość danych przestaje być problemem, ponieważ są one potencjalnie nieskończone i dostarczane do klasyfikatora w sposób ciągły. Z tego powodu użycie metody wstrzymania nie ma negatywnych skutków ubocznych.

Pojawiają się jednak nowe trudności. Po pierwsze skuteczność nie może być już dłużej wyrażona jedną liczbą. Ma ona jedynie logiczny sens w pewnym określonym momencie czasu. Większość dotychczasowych badań dokonuje ewaluacji klasyfikatorów za pomocą wykresu ilustrującego wskaźnik skuteczności w czasie. W oczywisty sposób utrudnia to porównywanie skuteczności różnych klasyfikatorów, ponieważ wykresy nie mają naturalnie określonego porządku liniowego. W celu określenia wyższości jednego klasyfikatora nad innym rozpatruje się takie właściwości jak tempo uczenia albo skuteczność uzyskana pod koniec eksperymentu.

Drugim, poważniejszym problemem jest zagadnienie zmieniającej się charakterystyki danych w czasie. Oznacza to, że nie tylko model tworzony przez klasyfikator zmienia się w czasie, ale również samo badane zjawisko ulega zmianom. Jednym z rozważanych sposobów radzenia sobie z tą sytuacją jest metoda \emph{snapshot}. Polega ona na stworzeniu kopii klasyfikatora w określonych momentach czasu. Kopie nie podlegają już dalszemu treningowi, jednak ich skuteczność na przyszłych danych może pokazać, że pierwotny klasyfikator faktycznie modeluje zmieniającą się charakterystykę. W niniejszej pracy tego typu strumienie nie są rozważane.\\

Dwiema najbardziej popularnymi metodami ewaluacji dla strumieni danych są:
\begin{itemize}
  \item \textbf{Metoda wstrzymania} (ang. \emph{Holdout method}) - Potencjalnym źródłem danych testowych są po prostu nowe dane przyjmowane przez system. W celu zmniejszenia obciążenia systemu ewaluacja może odbywać się w odstępach czasu. Co więcej po procesie przetestowania klasyfikatora na danych testowych, mogą one zostać ponownie użyte jako dane treningowe. W przypadku zagadnień, w których nie występuje zjawisko zmiany rozkładu w czasie (ang. \emph{concept drift}) można również zastosować jeden statyczny zbiór używany do ewaluacji skuteczności. Dzięki temu uzyskiwane w czasie wyniki mogą być wzajemnie porównywane.
  \item \textbf{Metoda testuj-potem-trenuj} (ang. \emph{test-then-train}) -  Alternatywnym podejściem jest połączenie procesu treningu i ewaluacji. Każda z instancji może w pierwszej kolejności zostać użyta do ewaluacji, a w drugiej kolejności do treningu. Warto zauważyć, że w tym wypadku model nadal zawsze testowany jest na danych na których nie był uczony. Zaletą tego podejścia jest fakt, że każdy test odbywa się na modelu stworzonym przy użyciu wszystkich dostępnych danych - pod tym względem ta metoda przypomina sprawdzian krzyżowy z pominięciem jednego elementu (ang. \emph{Leave-one-out}).

      Wadą tego podejścia jest fakt, że estymacja skuteczności będzie zaniżona przez początkowe błędy klasyfikatora, które wynikały z niedouczenia w początkowym okresie działania algorytmu. Początkowe błędy mają jednak coraz mniejsze znaczenie wraz z upływem czasu.
\end{itemize}

\section{Porównanie z metodami klasycznymi}

Część algorytmów \emph{machine learning} może zostać naturalnie zastosowana w sytuacji gdy chce się zachować możliwość dodatkowej nauki modelu w późniejszym czasie:
\begin{itemize}
  \item K-najbliższych sąsiadów,
  \item Naiwny klasyfikator bayesowski,
  \item Sztuczne sieci neuronowe.
\end{itemize}
W tych przypadkach również niezbędne może okazać się wprowadzenie pewnych usprawnień. Na przykład dla klasycznej wersji algorytmu K-najbliższych sąsiadów wymagane jest gromadzenie wszystkich zaobserwowanych dotychczas obserwacji, co jest niedopuszczalne przy ograniczonej pamięci.

Diagramy umieszczone na rysunkach \ref{fig:klasklas} i \ref{fig:strumklas} pozwalają na porównanie klasycznego zagadnienia klasyfikacji i występującego w przypadku strumieniowym.

\begin{figure}[h!]
  \centering
    \input{tikz/klasycznaKlasyfikacja.tex}
  \caption{Proces treningu i klasyfikacji w klasycznym przypadku uczenia maszynowego: klasyfikator uczy się na statycznym zbiorze danych.}
  \label{fig:klasklas}
\end{figure}

\begin{figure}[h!]
  \centering
    \input{tikz/strumieniowaKlasyfikacja.tex}
  \caption{Cykl treningu i klasyfikacji w przypadku uczenia klasyfikatora na danych strumieniowych. Klasyfikator może w dowolnym momencie dokonywać klasyfikacji lub być dalej uczony na danych treningowych.}
  \label{fig:strumklas}
\end{figure}

\begin{table}
\begin{center}
  \begin{tabular}{ | l | p{4cm} | p{3cm} | p{3cm} | }
    \hline
    Cecha & \emph{Machine learning} & \emph{Data Mining} & Zagadnienie strumieniowe \\ \hline

    Rozmiar danych &
          - niewielki &
          - duży &
          - bardzo duży \\
          \hline

    Zbiór danych &
          - statycznym &
          - statyczny &
          - dynamiczny \\
          \hline

    Dostęp do danych &
          - wielokrotny &
          - wielokrotny &
          - jednorazowy \\
          \hline

    Rozkład obserwacji &
          \pbox{4cm}{- niezależny \\ - identyczny} &
          \pbox{3cm}{- niezależny \\ - identyczny} &
          \pbox{3cm}{- zależny \\ - możliwe zmiany} \\
          \hline

    Proces nauki &
          - powtarzany &
          - powtarzany &
          - nieskończony \\
          \hline

    Ewaluacja &
          \pbox{4cm}{- sprawdzian krzyżowy \\ - \emph{leave-one-out} \\ - bootstrap } &
          - \emph{holdout} &
          \pbox{3cm}{- \emph{holdout} \\ - \emph{test-then-train}} \\
          \hline
  \end{tabular}
\end{center}
\caption{Tabela przedstawia porównanie klasycznych metod uczenia maszynowego: \emph{machine learning}
i \emph{data mining}, z zagadnieniem strumieniowym. Porównanie dotyczy zarówno charakterystyki zbioru danych,
ale również działania algorytmu i możliwych metod jego ewaluacji.}
\label{table:porownanie}
\end{table}

W tabeli \ref{table:porownanie} zostało przedstawione poglądowe porównanie ze sobą trzech przedstawionych charakterystyk. Na podstawie tej tabeli można zauważyć, że dowolny algorytm operujących na strumieniach spełnia również założenia stawiane zagadnieniom \emph{data mining} i \emph{machine learning}.

\input{chapters/hoeffding.tex}
