\chapter{Wybór cech dla strumieniowego klasyfikatora}

Głównym celem niniejszej pracy jest opisanie metody klasyfikacji działającej na strumieniach danych, dokonującej selekcji atrybutów w trakcie swojego działania. Możliwe są różne zastosowania takiego algorytmu - na przykład w przypadku budowy klasyfikatora na podstawie cech, których wpływ na klasę wyników jest jedynie potencjalny albo względem siebie redundantny. Innym potencjalnym zastosowaniem jest redukcja rozmiaru danych analizowanych przez klasyfikator i przetwarzanych przez system - dzięki wybraniu jedynie cech istotnych i biorąc pod uwagę potencjalny długi czas działania systemu może prowadzić to do znacznych zysków wydajnościowych.

\begin{defi}[Zagadnienie selekcji atrybutów dla klasyfikatora]
  Zagadnienie selekcji atrybutów dla klasyfikatora polega na określeniu podprzestrzeni $\mathcal{X'}$ dla przestrzeni atrybutów $\mathcal{X}$ przy jednoczesnym zachowaniu jego skuteczności.
\end{defi}

Rozważanie zagadnienia selekcji atrybutów ma najwięcej sensu przy założeniu, że niektóre z atrybutów nie są istotne względem badanej cechy lub są redundantne z innymi dostarczonymi atrybutami. Zagadnienie selekcji atrybutów można rozważać również w przypadku strumieniowych klasyfikatorów. Należy wtedy pamiętać o wszystkich wymaganiach stawianych algorytmom działającym na strumieniach danych, wyszczególnionych w sekcji \ref{wymagania}.

\section{Selekcja atrybutów w przypadku klasycznym}

Do selekcji atrybutów w przypadku klasycznych metod uczenia maszynowego najczęściej stosowane są dwie metody:
\begin{enumerate}
  \item Metoda \emph{obwoluty} (ang. \emph{wrapper method})
  \item Wybór na podstawie korelacji
\end{enumerate}

\subsection{Metoda \emph{obwoluty}}

W tej metodzie stosowany jest określony klasyfikator, który podlega wielokrotnej ewaluacji na danych testowych w celu określeniu podzbioru $\mathcal{X'}$. W skrajnym przypadku metoda ta wykonuje $2^n$ ewaluacji dla każdego możliwego podzbioru atrybutów (gdzie $n = dim(X)$). Ze względu na czasochłonność takiego procesu najbardziej rozpowszechnioną wersją tej metody jest jej wariant zachłanny, który występuje w trzech wersjach:
\begin{itemize}
  \item przeszukiwania w przód,
  \item przeszukiwania w tył,
  \item przeszukiwania dwustronnego.
\end{itemize}

W przypadku przeszukiwania w tył w pierwszej kolejności algorytm zostanie sprawdzony przy użyciu danych testowych używając do treningu wszystkich atrybutów. Następnie zostanie wykonane $n$ kolejnych ewaluacji modelu powstałego poprzez wytrenowanie klasyfikatora na danych, z których usunięty został $i$-ty atrybut, dla $i \in \{1,\dots,n\}$, starających się poprawić uzyskany wynik. Jeśli przynajmniej jeden z nowostworzonych klasyfikatorów okaże się lepszy, wybierany jest najlepszy z nich i proces jest kontynuowany, tzn. następuje próba usunięcia kolejnego z $n-1$ atrybutów. Proces ten trwa aż nie zostanie znaleziony klasyfikator dla którego usunięcie żadnego z atrybutów nie powoduje polepszenia skuteczności na danych testowych.

\begin{figure}[h!]
  \centering
    \scalebox{.8}{\input{tikz/przeszukiwanieWTyl.tex}}
  \caption{Proces selekcji atrybutów dla metody \emph{obwoluty}. Selekcja może odbywać się w przód lub w tył, co powoduje odpowiednio dodawanie lub usuwanie atrybutów. W jednej iteracji algorytm dokonuje ewaluacji modelu na wszystkich możliwych zbiorach atrybutów, różniących się tylko jednym elementem. Jeżeli uda się osiągnąć lepszy wynik to taki zbiór staje się głównym i proces jest powtarzany.  Występuje również trzeci rodzaj szukania najlepszego zbioru atrybutów: przeszukiwanie dwustronne, które łączy oba wymienione sposoby.}
\end{figure}

Proces przeszukiwania w przód przebiega analogicznie: pierwszy klasyfikator nie korzysta z żadnego z atrybutów i w kolejnych iteracjach do zbioru atrybutów dodawany jest atrybut, dla którego ewaluacja przyniosła najlepszy wynik. Proces ten trwa tak długo jak istnieje atrybut, który uzyskuje lepszy wynik na danych testowych niż klasyfikator uzyskany w poprzedniej iteracji.

Górne oszacowanie liczby stworzonych klasyfikatorów w przypadku przeszukiwania w przód lub przeszukiwania w tył jest takie samo: $n + (n-1) + \ldots + 2 + 1 + 1 = 1+\frac{n(n+1)}{2}$. W tym przypadku wykonuje się więc asymptotycznie $O(n^2)$ ewaluacji klasyfikatora na danych testowych co jest znaczącą poprawą względem złożoności wykładniczej badającej wszystkie podzbiory atrybutów $O(2^n)$.

Ostatnim z wariantów jest przeszukiwanie dwustronne - w tym wypadku klasyfikator utworzony na podstawie atrybutów jest porównywany zarówno z klasyfikatorami z o jednym więcej atrybutem, jak i z o jednym mniej. W pesymistycznym przypadku znowu mogą zostać zbadane wszystkie możliwości, więc metoda ma złożoność wykładniczą. Zazwyczaj można oczekiwać złożoności zbliżonej do dwóch poprzednich metod, a jednocześnie znacznie zwiększona jest szansa algorytmu na omijanie lokalnych ekstremum w procesie przeszukiwania.

\subsection{Wybór na podstawie korelacji}
\label{metodakorelacji}

Zaletą tej metody jest brak potrzeby używania klasyfikatora w procesie selekcji. Dzięki temu klasyfikatory nie muszą być wielokrotnie trenowane na danych treningowych i porównywane ze sobą.

Metoda ta opiera się na następującej obserwacji: \emph{``Dobry zbiór atrybutów to taki, który zawiera atrybuty wysoko skorelowane z klasami obiektów, ale nie skorelowane między sobą.''} Miara $good: 2^\mathcal{X} \rightarrow \mathcal{R}$ jest wskaźnikiem jakości wybranego podzbioru atrybutów.

$$ good(\mathcal{Z}) = \frac {\sum_{X \in \mathcal{Z}}corr(X,Y)} { \sqrt{ \sum_{X_i \in \mathcal{Z}} \sum_{X_j \in \mathcal{Z}}corr(X_i,X_j) } } $$

Bardzo dużą zaletą tej metody, w porównaniu ze swoją poprzedniczką jest jej szybkość, nawet przy badaniu wszystkich możliwych podzbiorów atrybutów. Przykładowo dla danych o $n=10$ atrybutach i $m=1000$ obserwacjach, dla tej metody liczba operacji wyniesie:
$$ O(n^2m + 2^nn^2) \approx 100 000$$
W przypadku metody \emph{obwoluty} z użyciem klasyfikatora, który ma złożoność $O(nm)$ - co jest założeniem optymistycznym (złożoność liniowa względem rozmiaru danych wejściowych), liczbę operacji można oszacować jako:
$$ O(2^nO(nm))=O(nm2^n) \approx 10 000 000$$

W typowym przypadku metoda na podstawie wyboru korelacji będzie działać $O(\frac{m}{n})$ razy szybciej.

\section{Selekcja atrybutów w przypadku strumieniowym}

Najważniejszym aspektem przy selekcji atrybutów dla strumieni danych jest zachowanie wiedzy już zdobytej na podstawie danych, które nigdy ponownie nie będą dostępne. Z tego powodu w niniejszej pracy przyjęte zostało założenie, że procesu nauki nigdy nie trzeba zaczynać od początku. Drugim istotnym założeniem jest możliwość uzyskiwania predykcji od samego początku działania algorytmu.

W zaproponowanej metodzie spełnione są oba te wymagania poprzez utrzymanie poprawnego stanu klasyfikatora przez cały czas. Dzięki temu w dowolnym momencie możliwy jest dostęp do możliwie dobrych klasyfikacji.

\subsection{Metoda \emph{obwoluty}}

\subsubsection{Opis metody}

W zaproponowanej metodzie algorytm wykonuje zachłanne przeszukiwanie w przód. Dane wejściowe zawierają informację o dostępnych atrybutach $\mathcal{X}$ i klasach $\mathcal{Y}$.
Klasyfikator $C$ zaczyna pracę z pustym zbiorem wybranych atrybutów $\mathcal{X'}$. Dodatkowo utworzonych zostaje $n$ kopii klasyfikatora $C$ wykorzystujących o jeden atrybut więcej ze zbioru $\mathcal{X}$. Wszystkie klasyfikatory są poddawane ciągłej ewaluacji przy pomocy metody testuj-potem-trenuj (ang. \emph{test-then-train}). Co powien określony czas dochodzi do porównania wyników klasyfikatorów. Jeśli istnieje widoczna przewaga jednej z kopii, to klasyfikator $C$ jest nią zastępowany. Utworzony zostaje kolejny zestaw kopii (tym razem ich liczba jest o jeden mniejsza, ponieważ jeden z atrybutów dołączył do zbioru $\mathcal{X'}$). Cały proces jest powtarzany w kolejnych iteracjach i może zostać zakończony:
\begin{itemize}
  \item jeśli zostały wybrane wszystkie atrybuty ze zbioru $\mathcal{X}$
  \item jeśli przez wystarczająco długi czas nie odnotowano poprawy klasyfikatora $C$
\end{itemize}

Do działania algorytm potrzebuje dwóch następujących parametrów:
\begin{itemize}
  \item $\alpha$ - liczba obserwacji przetwarzanych w jednej iteracji algorytmu
  \item $\beta$ - wysokość przewagi w skuteczności kopii klasyfikatora, uzasadniająca zamianę
\end{itemize}

W przyszłości może okazać się wartościowe zbadanie potencjalnych domyślnych wartości dla tych parametrów. Opisana metoda została zapisana w formie pseudokodu jako algorytm \ref{algObwoluta}.

\begin{algorithm}
\caption{Pseudokod funkcji składających się na implementację metody \emph{obwoluty}. Funkcje $\textsc{StworzKandydatow}$ i $\textsc{Ewaluacja}$ mają charakter pomocniczy. Wszystkie cztery funkcje działają w stałej złożoności czasowej.}
\label{algObwoluta}
\begin{algorithmic}
\State C $\gets$ NowyKlasyfikator()
\State $\text{C.atrybuty} \gets \varnothing$
\State \Call{StworzKandydatow}{$ $}
\State $\text{licznik} \gets 0$
\State
\Function{StworzKandydatow}{$ $}
  \State $\text{kandydaci} \gets \varnothing$
  \ForAll{$X \in \mathcal{X} \setminus \text{C.atrybuty}$}
    \State $\text{kandydat} \gets \text{C.kopia()}$
    \State $\text{kandydat.atrybuty} \gets \text{C.atrybuty} \cup X$
    \State $\text{kandydaci} \gets \text{kandydaci} \cup \text{kandydat}$
  \EndFor
\EndFunction
\State
\Function{Ewaluacja}{$ $}
  \State $\text{najlepszy} \gets \argmax_{\text{K} \in \text{kandydaci}} \text{K.wynik} $
  \If{$\text{najlepszy.wynik} - \text{C.wynik} > \beta$}
    \State C$ \gets \text{najlepszy}$
    \State C.wyczyscWynik()
    \State \Call{StworzKandydatow}{$ $}
  \EndIf
\EndFunction
\State
\Function{Trenuj}{$\vec{x},y$}
  \State C.testujPotemTrenuj($\vec{x},y$)
  \ForAll{$\text{kandydat} \in \text{kandydaci}$}
    \State $\text{kandydat.testujPotemTrenuj}(\vec{x},y)$
  \EndFor
  \State $\text{licznik} \gets \text{licznik} + 1$
  \If{$\text{licznik} = \alpha$}
    \State $\text{licznik} \gets 0$
    \State \Call{Ewaluacja}{$ $}
  \EndIf
\EndFunction
\State
\Function{Klasyfikuj}{$\vec{x}$}
  \State \Return C.klasyfikuj($\vec{x}$)
\EndFunction
\end{algorithmic}
\end{algorithm}

\subsubsection{Analiza złożoności}

Z wymagań stawianych przed strumieniowymi klasyfikatorami wiadomo, że działają one w stałej złożoności pamięciowej, a klasyfikacja i uczenie się przebiega w czasie stałym względem jednej obserwacji.

Zaproponowany algorytm adaptacyjny wymaga stworzenia $n$ dodatkowych klasyfikatorów, gdzie $n$ to liczba atrybutów. W rozważanym przypadku liczba atrybutów jest stała. Rozmiar jednej przetwarzanej obserwacji wynosi $O(n)$ i w praktycznych zastosowaniach jest niewielki.
Klasyfikacja odbywa się w czasie stałym ponieważ wykonuje się ona bezpośrednio na klasyfikatorze $C$.
W procesie uczenia się dla każdego klasyfikatora dokonywana jest ewaluacja i trening na obserwacji. Obie operacje mają stałą złożność czasową, więc również nauka odbywa się w czasie stałym.

\subsection{Możliwe modyfikacje algorytmu}

Przedstawiony algorytm można rozbudowywać albo przebudowywać w zależności od pojawiających się wymagań. Potencjalne ścieżki rozwoju, to:
\begin{itemize}
  \item Wykorzystanie metoda wstrzymania (ang. \emph{holdout}) do ewaluacji wszystkich klasyfikatorów.
  \item Dokonywanie ewaluacji atrybutów nie na podstawie kopii klasyfikatorów, ale z wykorzystaniem metody wyboru na podstawie korelacji, wyjaśnionej w sekcji \ref{metodakorelacji}.
  \item Przeszukiwanie w tył bądź w dwie strony. Oba warianty zachłanne stają się możliwe do użycia jeśli wykorzystany strumieniowy klasyfikator będzie zezwalał na usuwanie atrybutów (jak np. naiwny klasyfikator bayesowski).
  \item Optymalizacja wydajności poprzez wykonywanie obliczeń dla kopii klasyfikatora w sposób rozproszony.
  \item Przeszukiwanie zbioru atrybutów nie jednocześnie, a kolejno. Dzięki takiemu podejściu jest możliwe utworzenie tylko jednej kopii klasyfikatora. Wadą w takim wypadku jest fakt, że proces uczenia potencjalnych kopii zachodzi na innych zbiorach danych. Zaletą jest zmniejszenie wymagań pamięciowych i mniejsza ilość obliczeń.
\end{itemize}

\subsection{Ewolucyjne strumienie danych}

Przedstawiony algorytm nie jest przystosowany do pracy ze strumieniami danych zmieniającymi swój rozkład w czasie i wymagałby adaptacji. W tym wypadku należy oczywiście zastanowić się nad wymaganiami stawianymi przed takim algorytmem.

W skrajnym przypadku cecha, które w pewnym momencie okazała się kluczowa do budowy klasyfikatora może okazać się całkowicie zbędna lub odwrotnie. Z tego powodu niezbędne staje się użycie przeszukiwanie dwustronnego.




