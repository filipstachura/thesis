\chapter{Metody klasyczne}

\section{Wprowadzenie}
Uczenie maszynowe jest dziedziną nauki zajmującą się tworzeniem i badaniem algorytmów, które analizują i uczą się wyciągać wnioski z danych. Tego typu algorytmy budują abstrakcyjne modele pozwalające im wnioskować i podejmować decyzje bez wykorzystania zaprogramowanej wcześniej logiki. Tom Mitchell zdefiniował \emph{machine learning} następująco: ``Mówimy, że program komputerowy uczy się na doświadczeniu \emph{E} w stosunku do pewnej klasy zadań \emph{T} i miary skuteczności \emph{P}, jeśli jego skuteczność, mierzona przez \emph{P}, dla zadań z \emph{T} poprawia się wraz z doświadczeniem \emph{E}'' \cite{mitchell}.

W ostatnich dziesięcioleciach klasyczne metody uczenia maszynowego były intenstywnie poprawiane. Niewielka ilość dostępnych danych zmuszała badaczy do tworzenia algorytmów maksymalnie je wykorzystujących. Powstały również sposoby mające na celu zwielokrotnienie zbioru danych, takie jak np. \emph{bootstraping}. Większość stworzonych metod oparta jest na zachłannej metodzie \emph{hill climbing} przeszukującej przestrzeń modeli.

Wraz ze wzrostem ilości dostępnych danych, metody te zostały uogólnione. Metody \emph{data mining} i \emph{big data} mają na celu radzenie sobie z dużymi zbiorami danych, jednak kluczowe charakterystyki zbioru zostają te same: zbiór jest skończony i statyczny, o jednakowym i niezależnym rozkładzie.

\section{Przykłady badanych zagadnień}

Przytoczona powyżej definicja jest ogólna, a zbiór metod i problemów badanych w ramach tej dziedziny bardzo rozległy. Częstą metodą rozróżniania problemów jest podział ze względu na odmienną charakterystykę zbioru danych. W takim wypadku rozróżnia się:
\begin{itemize}
  \item \textbf{Uczenie z nadzorem} (ang. \emph{Supervised learning}) - w którym dostarczony zbiór przykładów, zawiera dla danych wejściowych, dopasowane - właściwe dane wyjściowe.
  \item \textbf{Uczenie bez nadzoru} (ang. \emph{Unsupervised learning}) - w którym dostarczone zostają jedynie dane wejściowe, pozostawiając odkrycie ich struktury, jako problem otwarty. Uczenie bez nadzoru może być więc użyte, jako metoda pośrednia w większym problemie, lub jako narzędzie do odkrycia zależności w danych.
\end{itemize}

Inny popularny podział zadań skupia się na wyniku działania algorytmu:
\begin{itemize}
  \item \textbf{Zagadnienie klasyfikacji} (ang. \emph{Classification}) - Algorytm uczy się na danych wejściowych, które są podzielone na ustalone klasy. Wynikiem algorytmu jest model pozwalający przyporządkować klasę z zadanego zbioru - dla obserwacji, na których nie jest ona określona. Ten problem najczęściej wykorzystuje uczenie z nadzorem.
  \item \textbf{Zagadnienie klasteryzacji} (ang. \emph{Cluster analysis}) - dane należy podzielić na logiczne grupy. Ponieważ są one nieznane przed działaniem algorytmu jest to typowy przypadek uczenia bez nadzoru.
  \item \textbf{Redukcja wymiarów} (ang. \emph{Dimensionality reduction}) - wynik to odwzorowanie danych na przestrzeń o mniejszym wymiarze (tzn. np. zamiast dziesięciu atrybutów dalszej analizie mogą podlegać tylko trzy).
  \item \textbf{Estymacja rozkładu} - wynikiem działania jest estymacja rozkładu danych w pewnej przestrzeni.
\end{itemize}

Istnieją również zagadnienia, które ciężko przyporządkować do którejkolwiek z tych kategorii. Na przykład problem \textbf{popularnego podzbioru} (ang. \emph{frequent item set mining}), który polega na znalezieniu podzbiorów w danych, które często występują jednocześnie. Metoda ta może być użyta np. do stwierdzenia, jaki produkt można zaproponować klientowi na podstawie już dokonanych przez niego zakupów.

Kolejne rozdziały pracy koncentrują się na analizie zagadnienia klasyfikacji w klasycznym przypadku uczenia z nadzorem, ale w sytuacji kiedy źródło danych jest strumieniowe. W tym rozdziale omówiony został przypadek statycznego źródła danych, aby potem mogła być przedstawiona różnica pomiędzy tymi dwoma podejściami.

% poniższa mi się nie podoba. Może  "Kolejne rozdziały pracy skupiają..." ? kolejne zdanie połącz z tym i usuń "główna różnica", zamiast tego daj: ", ale w przypadku, kiedy źródło danych jest strumieniowe. "

\section{Zagadnienie klasyfikacji}

W przypadku klasycznym zagadnienie klasyfikacji polega na stworzeniu modelu, który przewiduje klasę niezidentyfikowanego obiektu na podstawie treningu na danych dla których klasa została określona.

\begin{defi}[Zagadnienie klasyfikacji]
  Dla danego zbioru danych treningowych\\
  $\{(\vec{x_1},y_1),\dots\,(\vec{x_m},y_m)\}$, gdzie $\forall_{i \in 1\dots m}\vec{x_i} \in \mathcal{X}$ oraz $\forall_{i \in 1\dots m}y_i \in \mathcal{Y}$, znaleźć klasyfikator, tj.~funkcję $\mathcal{X}\rightarrow\mathcal{Y}$,
  który przydziela dowolnemu obiektowi $\vec{x} \in \mathcal{X}$ klasę $y \in \mathcal{Y}$.
\end{defi}

Przykładem może być klasyfikator, który na podstawie informacji o stanie pacjenta ($\vec{x} \in \mathcal{X}$)
stawia diagnozę czy pacjent jest chory czy nie ($y \in \mathcal{Y}$).

W następujących sekcjach zostaną opisane dwa popularne klasyfikatory. Ich znaczenie praktyczne przy zastosowaniach uczenia maszynowego jest obecnie mniejsze na skutek rozwoju bardziej wyrafinowanych i skuteczniejszych metod. Stanowią one jednak istotną bazę do dalszych rozważań.

\subsection{Naiwny klasyfikator bayesowski}

Naiwny klasyfikator bayesowski jest klasyfikatorem probabilistycznym, opierającym się na silnym założeniu o wzajemnej niezależności atrybutów i wykorzystującym twierdzenie Bayesa. W praktyce założenie to jest zazwyczaj fałszywe, mimo to klasyfikator sprawdza się zaskakująco dobrze przy zastosowaniach.

\begin{tw}[Prawdopodobieństwo całkowite]
\label{pcalk}
  Dla danej przestrzeni probabilistycznej ($\Omega$,$\Sigma$,$P$) oraz zdarzeń $U_1,\dots,U_n \in \Sigma$ spełniających warunki:
  \begin{itemize}
    \item $\forall_{i \in 1 \dots n} P(U_i) > 0$
    \item $\forall_{i,j \in 1 \dots n} (i \neq j) \implies U_i \cap U_j = \varnothing $
    \item $\bigcup_{i \in 1 \dots n} U_i =\Omega$
  \end{itemize}
  dla każdego zdarzenia $B \in \Sigma$ zachodzi zależność:
    $$ P(B) = \sum_{i=1}^n{P(B|U_i)P(U_i)} $$
\end{tw}

\begin{tw}[Twierdzenie Bayesa]
  Przy założeniach jak w Twierdzeniu \ref{pcalk} zachodzą następujące równości:
    $$ \forall_{k \in 1 \dots n} P(U_k|B) = \frac{P(B|U_k)P(U_k)}{\sum_{i=1}^n{P(B|U_i)P(U_i)}}$$
\end{tw}

Model prawdopodobieństwa dla klasyfikatora jest modelem warunkowym. Niech $Y$ będzie zmienną losową klasy, a $X_1,\dots,X_n$ będą zmiennymi losowymi dla poszczególnych atrybutów. Korzystając z Twierdzenia Bayesa można zapisać go w następujący sposób:
$$ P(Y|X_1,\dots,X_n) = \frac{P(X_1,\dots,X_n|Y)P(Y)}{P(X_1,\dots,X_n)}$$

W praktyce, dla każdej z klas $y \in \mathcal{Y}$ mianownik ma stałą wartość, ponieważ zależy wyłącznie od wartości atrybutów danej instancji. Z niezależności atrybutów prawdopodobieństwo warunkowe z licznika można zapisać w wygodniejszej formie iloczynu:
$$ P(Y|X_1,\dots,X_n) = cP(Y)\prod_{i=1}^nP(X_i|Y)$$

Gdzie $c$ jest stałą, niezależną od rozważanej klasy. Ostatecznie algorytm wybiera klasę dla obserwacji $(x_1,\dots, x_n) = \vec{x} \in \mathcal{X}$ w następujący sposób:
$$  \text{classify}(\vec{x}) = \argmax_{y \in \mathcal{Y}} P(Y=y)\prod_{i=1}^nP(X_i=x_i|Y=y)$$

Pseudokod ilustrujący działanie klasyfikatora został zapisany jako algorytm \ref{algNB}.

\begin{algorithm}
\caption{Implementacja naiwnego klasyfikatora bayesowskiego.}
\label{algNB}
\begin{algorithmic}
\State $\text{klasa} \gets [0, 0, \dots, 0]$
\State $\forall_{i \in dim(\mathcal{X})}~\text{tablica}_i \gets [[0, 0, \dots, 0],\dots,[0, 0, \dots, 0]]$
\State
\Function{Trenuj}{$(\vec{x_1},y_1),\dots,(\vec{x_m},y_m)$}
\State $i\gets 1$
  \While{$i \le m$}
    \State $(x_1,\dots,x_n) \gets \vec{x_i}$
    \State $\text{klasa}[y_i] \gets \text{klasa}[y_i] + 1$
    \ForAll{$j \in 1,\dots,n$}
      \State $\text{tablica}_j[y_i][x_j] \gets \text{tablica}_j[y_i][x_j] + 1$
    \EndFor
    \State $i \gets i + 1 $
  \EndWhile
\EndFunction
\State
\Function{Klasyfikuj}{$\vec{x}$}
  \State $(x_1,\dots,x_n) \gets \vec{x}$
  \State \Return $\argmax_{y \in \mathcal{Y}} \frac{\text{klasa}[y]}{m}\prod_{i=1}^n{\frac{\text{tablica}_i[y][x_i]}{\text{klasa}[y]}}$
\EndFunction
\end{algorithmic}
\end{algorithm}

\subsection{Klasyfikator C4.5}
\label{sec:c45}

Algorytm ten został zaproponowany po raz pierwszy przez Rossa Quinlana w 1993 roku \cite{quinlan}.
Wynikiem jego działania jest drzewo decyzyjne, które następnie może być używane do klasyfikacji. Drzewo decyzyjne można formalnie zdefiniować jako skierowany graf acykliczny (\emph{DAG}, ang. \emph{Directed acyclic graph}), w którym każdy wierzchołek jest albo liściem albo węzłem decyzyjnym z przynajmniej dwójką dzieci. W najprostszej wersji drzewa decyzyjnego, każdemu liściowi przypisana jest pewna klasa, a w węzłach decyzyjnych zapisany jest test logiczny bazujący na wartości jednego atrybutu.

Drzewa decyzyjne zawdzięczają swoją popularność dużej czytelności stworzonego modelu, pozwalającej na dodatkową jego interpretację.

W celu wyjaśnienia działania algorytmu budującego drzewo C4.5 niezbędne jest zdefiniowanie \emph{zysku informacyjnego}.
To pojęcie i niezbędne pojęcia pomocnicze zdefiniowane są następująco:

\begin{defi}[Entropia rozkładu prawdopodobieństwa]
  Dla danego rozkładu prawdopodobieństwa $P = (p_1,\dots,p_n)$ entropią nazywamy wartość: $ H(P) = -\sum_{i=1}^n{p_{i}\textit{log}(p_i)}$
\end{defi}

\begin{defi}[Informacja zbioru]
Bazując na entropii została zdefiniowana ilość informacji dowolnego, skończonego zbioru $T$ podzielonego na rozłączne klasy $C_1,\dots,C_k$ jako:
$\textit{Info}(T) = H(P)$ gdzie $ P=(\frac{|C_1|}{|T|},\dots,\frac{|C_k|}{|T|})$
\end{defi}

\begin{defi}[Informacja atrybutu]
Niech $X$ będzie wybranym atrybutem, który dzieli zbiór $T$ według swoich wartości na podzbiory $T_1,\dots,T_k$. Ilość informacji względem tego atrybutu definiujemy jako:
$ \textit{Info}(X, T) = \sum_{i=1}^k{\frac{|T_i|}{|T|}\textit{Info}(T_i)}$
\end{defi}

\begin{defi}[Zysk informacyjny (ang. \emph{Information gain})]
Na koniec zdefiniujmy zysk informacyjny: $\textit{Gain}(X, T) = \textit{Info}(T) - \textit{Info}(X, T)$. Można go interpretować jako informację potrzebną do stwierdzenia klasy elementu z $T$ po poznaniu wartości atrybutu $X$.
\end{defi}

\emph{Zysk informacyjny} jest wykorzystywany przez algorytm C4.5 do budowy drzewa. W węzłach decyzyjnych test logiczny jest tworzony poprzez wybranie atrybutu dla którego wartość $\textit{Gain}$ jest najwyższa\footnote{Teoretycznie możliwy jest też wybór innych metryk takich jak np. \emph{Gini index}}. Algorytm działa rekurencyjnie dla wszystkich węzłów drzewa i  zatrzymuje się w jednym z następujących przypadków:
\begin{itemize}
  \item W zbiorze występują tylko elementy jednej klasy - w tym wypadku tworzony jest liść, który będzie klasyfikował obiekty jako tę klasę
  \item Żaden z atrybutów nie powoduje wzrostu informacji - w tym wypadku również tworzony jest liść, ale jego etykieta to rozkład klas na tym zbiorze
\end{itemize}

\begin{algorithm}
\caption{Rekurencyjny algorytm budujący drzewo C4.5 na podstawie zbioru danych treningowych.}
\label{algC45}
\begin{algorithmic}
\Function{BudujDrzewoC45}{Dane, drzewo}
\If{kryteriumStopu(Dane)}
  \State drzewo.etykieta $\gets$ kategoria(Dane)
  \State \Return drzewo
\EndIf
\State j$ \gets \argmax_{i \in 1\dots n} \textit{Gain}(X_i, \text{Dane})$
\State drzewo.test $\gets X_{j}$
\ForAll{$\text{wartosc} \in X_{j}$}
  \State $\text{Dane}' \gets \{x \in \text{Dane} | X_{j}(x) = \text{wartosc}\}$
  \State $\text{drzewo.dodajDziecko}(\textsc{BudujDrzewoC45}(\text{Dane}', \text{noweDrzewo}()))$
\EndFor
\State \Return $t$
\EndFunction
\end{algorithmic}
\end{algorithm}

W algorytmie C4.5 zastosowano jeszcze inne usprawnienia, zwiększające jego możliwości i skuteczność. Quinlan w swojej pracy zaproponował również często stosowaną metodę przycinania. W kolejnych pracach opisał również metodę obsługi atrybutów numerycznych \cite{quinlan2}. Uniwersalność i czyteloność tego modelu sprawiły, że drzewa C4.5 stały się jedną z podstawowych metod uczenia maszynowego.

\section{Metody ewaluacji jakości modelu}

W pracy przyjętym założeniem jest, że najważniejszym czynnikiem względem którego dokonywana jest ocena algorytmu uczącego się jest skuteczność klasyfikacji. Równoważnie można rozpatrywać jej przeciwieństwo: błąd. Zazwyczaj jest on mierzony jako ułamek błędnych klasyfikacji na zbiorze danych testowych.

Ogólnie przyjętym założeniem jest sprawdzanie skuteczności modelu na danych innych niż treningowe. Jednym z podstawowych
wymogów stawianych algorytmom uczącym się jest zdolność generalizacji i właściwej predykcji dla obserwacji, które bezpośrednio nie zostały
zaobserwowane. Dodatkowo pozwala to uniknąć zjawiska \emph{przeuczenia} na które jest podatne wiele algorytmów, kiedy to klasyfikator za bardzo dopasowuje się do danych treningowych i dużo gorzej sprawdza się w innych przypadkach. \\

W klasycznym uczeniu maszynowym zbiór danych jest z założenia niewielki, więc metody ewaluacji skupiają się na możliwie efektywnym wykorzystaniu całego zbioru. Najczęściej spotykanymi metodami stosowanymi w klasycznych zastosowaniach są:
\begin{itemize}
  \item \textbf{Metoda wstrzymania} (ang. \emph{Holdout method}) -  Metoda \emph{holdout} jest najprostsza i polega na podzieleniu zbioru danych na dwie grupy: treningową i testową. Pierwsza z grup służy wyłącznie do trenowania klasyfikatora i nie jest brana pod uwagę w dalszym procesie ewaluacji. Na drugiej z grup dokonywana jest predykcja i wyliczenie estymacji skuteczności. Zwyczajowo przyjmowanym współczynnikiem podziału danych jest $\frac{2}{3}$ do $\frac{1}{3}$. Metodzie tej zarzucane jest nieefektywne wykorzystanie dostępnych danych. Kolejnym problemem jest fakt, że estymacja skuteczności klasyfikatora może znacznie się różnić w zależności od wybranego podziału zbioru danych. Ten efekt często jest minimalizowany poprzez wielokrotne powtarzanie testu przy innym podziale zbioru danych. Zaletą takiego podejścia jest zarówno poprawa estymacji skuteczności, ale także możliwość zbadania wariancji uzyskiwanych wyników.

  \item \textbf{Sprawdzian krzyżowy} (ang. \emph{Cross validation}) -  Aby zaradzić problemom dotyczącym poprzedniej metody często wybierana jest metoda sprawdzianu krzyżowego. W przypadku $k$-krotnej walidacji zbiór dostępnych danych dzielony jest na $k$ niezależnych, rozłącznych części o możliwie równej wielkości. Proces ewaluacji powtarzany jest dla każdej z części: wszystkie pozostałe zostają użyte do treningu klasyfikatora. Ostateczny estymator skuteczności bierze pod uwagę estymację na każdym elemencie zbioru (co jest równoważne średniej ze skuteczności klasyfikatorów na częściowych danych).

      Problemem, który dotyczy podstawowej wersji tej metody jest losowy i potencjalnie nie zbalansowany podział na części. W patologicznym przypadku można wyobrazić sobie jeden zbiór danych mający wszystkie obiekty klasy $A$, która w pozostałych częściach nie jest spotykana. W celu ograniczenia negatywnego efektu przekrzywienia danych stosuje się kroswalidację stratyfikowaną. Polega ona na możliwie równomiernym podziale klas wewnątrz części, tak aby każda z części możliwie poprawnie odzwierciedlała globalny rozkład.

      Dodatkowo proces kroswalidacji również może być powtarzany, za każdym razem z innym podziałem na rozłączne części. Dzięki temu może zostać policzona wariancja obserwowanego błędu.

  \item \textbf{Sprawdzian krzyżowy ``pozostaw jednego''} (ang. \emph{Leave-one-out}) - Jest to brzegowy przypadek sprawdzianu krzyżowego, w którym każdy element zostaje uznany za osobną część. Dla każdej instancji zostaje stworzony klasyfikator wytrenowany na pozostałych danych, który następnie testowany jest tym jednym obiektem.

      Zaletą tej metody jest brak czynnika losowego występującego w przypadku podziału na większe zbiory. Niestety stratyfikacja nie jest w tym wypadku możliwa. Najważniejszym problemem jest ostatecznie długi czas potrzebny na tworzenie poszczególnych klasyfikatorów. Mimo, że metoda często daje wiarygodne wyniki, w zastosowaniach praktycznych może być jedynie stosowana w wypadku modeli, które szybko mogą pominąć jeden z elementów użytych w trakcie uczenia.

  \item \textbf{Metoda bootstrap} (ang. \emph{Bootstrap}) - W tej metodzie tworzony jest nowy zbiór treningowy. Zbiór jest tego samego rozmiaru, a jego elementy są losowane ze zwracaniem ze zbioru oryginalnego. Prawdopodobieństwo dla pojedynczej obserwacji pozostania w zbiorze testowym wynosi około $0.63$. Prawdopodobieństwo bycia wylosowanym w jedej próbie wynosi $1/n$, a nie bycia wylosowanym $1-1/n$. Ponieważ wszystkie losowania są niezależne od siebie, to prawdopodobieństwo nie bycia wylosowanym podczas wszystkich $n$ losowań wynosi $(1-\frac{1}{n})^n \xrightarrow{\infty} e^{-1} \approx 0.368$. Szereg ten zbiega bardzo szybko, dlatego już dla małych wartości $n$ oszacowanie wartości przez granicę szeregu jest uzasadnione.

  Wszystkie niewylosowane elementy zostają użyte jako zbiór testowy, który średnio zawiera około $36.8\%$ zbioru danych.

      W metodzie tej ostateczną miarą skuteczności klasyfikatora jest współczynnik skuteczności na danych treningowych i testowych:
      $$ \text{acc}(\text{data}) = 0.368 * \text{acc}(\text{train}) + 0.632 * \text{acc}(\text{test}) $$

      Podobnie jak w powyższych metodach, metoda bootstrap również może być użyta wielokrotnie w celu uzyskania bardziej wiarygodnych estymacji i wariancji błędu.

      Należy również pamiętać, że miara skuteczności tej metody jest jedynie przybliżeniem i sama również jest podatna na błędy. Przykład błędnej estymacji można rozważyć czysto teoretycznie i traktować jako przestrogę przed pochopną interpretacją wyników metody bootstrap. Niech zbiór danych będzie całkowicie losowy z występującymi dwiema możliwymi klasami. Zbiór atrybutów nie jest istotny. Prawdziwa skuteczność dowolnego modelu na tych danych może wynosić w tym wypadku co najwyżej $50\%$. Rozważmy ewaluację klasyfikatora, który zapamiętuje dane treningowe, a dla danych nie zapamiętanych odpowiada losowo. Oczywiście na danych treningowych taki klasyfikator nie popełni żadnych błędów. Uzyskana ocena metodą bootstrap wyniesie: $$\text{acc}(\text{data}) = 0.368 * 1 + 0.632 * 0.5 = 0.684$$

\end{itemize}

Najczęściej stosowaną metodą w badaniach naukowych jest kroswalidacja stratyfikowana z podziałem zbioru na 10 części. Często wykonuje się też ewaluację różnymi metodami, w celu uwiarygodnienia uzyskanych wyników.
