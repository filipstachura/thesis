\section{Drzewo Hoeffdinga}

W ostatniej sekcji tego rozdziału zamieszczono opis działania klasyfikatora opartego o drzewo Hoeffdinga. Następnie przedstawiony został dowód, pokazujący że drzewo tworzone przez algorytm strumieniowy jest asymptotycznie bardzo bliskie drzewu \emph{C4.5} tworzonemu w sposób klasyczny.

Algorytm uczący się tworzący drzewo Hoeffdinga buduje je w sposób przyrostowy, jednorazowo badając pojedynczy przypadek w strumieniu. Algorytm nie zapisuje informacji na temat badanej obserwacji po fakcie wykorzystania jej do aktualizacji drzewa.
Jedyną informacją zachowaną w pamięci przez algorytm jest samo drzewo, które wykorzystuje informacje z liści w procesie dalszej aktualizacji. Drzewo to może zostać wykorzystane w dowolnym momencie do dokonania predykcji.

Algorytm w swoim działaniu jest zbliżony do wytłumaczonego w sekcji \ref{sec:c45} klasyfikatora C4.5, który również operuje na drzewie decyzyjnym. W obecnym przypadku również używana jest metryka \emph{zysku informacyjnego} w celu wybrania testów w wierzchołkach.

W przypadku uczenia klasycznego decyzja o wyborze atrybutu jest prosta: wystarczy wybrać atrybut o najwyższym \emph{zysku informacyjnym} na całości danych treningowych. Do dokonania wyboru w przypadku strumieniowym można użyć nierówności poraz pierwszy podanej przez Hoeffdinga \cite{hoeffdingbound}. Domingos i Hulten wykorzystali ją w swoich pracach, podając warunek dla klasyfikatora strumieniowego oraz dowodząc, że tak zbudowane drzewo będzie bliskie drzewu nauczonemu na pełnym zbiorze danych \cite{domingos}.

Nierówność Hoeffdinga mówi, że z prawdopodobieństwem $1-\delta$ średnia zmiennych losowych o wartościach na przedziale $R$ nie będzie różnić się od średniej empirycznej obliczonej na podstawie $n$ niezależnych obserwacji o więcej niż:
$$\epsilon = \sqrt{\frac{R^2ln(\frac{1}{\delta})}{2n}}$$

Ograniczenie to jest szczególnie przydatne, ponieważ nie zależy od rozkładu zmiennych losowych, a jedynie od przedziału wartości zmiennych, liczby obserwacji i zadanego przedziału ufności. Pewną wadą ograniczenia może być jego ogólność, ponieważ przy uwzględnieniu rozkładów zmiennych można uzyskać silniejsze ograniczenia \cite{jin}. W wypadku strumieniowym rozkład zmiennych nie jest znany, a ograniczenie zaproponowane przez Hoeffdinga sprawdza się dobrze w praktyce, dlatego silniejsze ograniczenia nie będą dalej omawiane.

Algorytm uczący się budujący drzewo Hoeffdinga w celu podjęcia decyzji dla którego atrybutu stworzyć test bada zmienną losową różnicy pomiędzy \emph{zyskiem informacyjnym} dla dwóch najlepszych atrybutów. Przykładowo: jeśli estymacja tej różnicy wynosi $v$ i jednocześnie $\epsilon < v$ to wnioskiem z nierówności Hoeffdinga jest, że prawdziwa różnica może wynosić w najgorszym wypadku $v - \epsilon > 0$. Ponieważ wartość ta jest dodatnia uzasadniony jest wniosek o wyższości najlepszego atrybutu nad pozostałymi.

W przypadku zmiennych jakościowych przedział wartości $R$ jest dla \emph{zysku informacyjnego} równy wartości logarytmu o podstawie $2$ z liczby możliwych klasy. Przy ustalonym $R$ i $\delta$ jedyną zmienną pozostaję $n$, wraz z którego wzrostem wartość $\epsilon$ maleje. Prosty test pozwala na podjęcie decyzji, z prawdopodobieństwem $1-\delta$, o wyborze dominującego atrybutu: najlepszy atrybut powienien być wybrany gdy różnica pomiędzy dwiema najlepszymi wartościami przekroczy wartość $\epsilon$. Jest to kluczowe założenie algorytmu uczącego się, którego pseudokod został zapisany jako algorytm~\ref{algHT}.

\begin{algorithm}
\caption{Budowa drzewa Hoeffdinga na podstawie strumienia danych.}
\label{algHT}
\begin{algorithmic}
\Function{BudujDrzewoHoeffdinga}{$S\text{: strumien danych}$}
\State HT$ \gets$ PustyLisc()
\ForAll{$(x,y) \in S$}
  \State l $\gets$ HT.przejdzDoLiscia(x)
  \State l.aktualizujStatystyki(x)
  \State l.licznik $\gets$ l.licznik$~+ 1$
  \If{$y == ?$}
    \State l.klasyfikuj(x)
  \Else
    \If{$ \text{l.licznik} \equiv 0 \Mod{n_{min}}$}
      \State Oblicz zysk informacyjny $\textit{Gain}_l(X_i)$ dla każdego z atrybutów
      \State Niech $X_a, X_b$ będą odpowiednio najlepszym i kolejnym atrybutem
      \State $\epsilon \gets \sqrt{ \frac{R^2ln(1/\delta)}{2n} }$
      \If{$ Gain_l(X_a) - Gain_l(X_b) > \epsilon \text{ lub } \epsilon < \tau$}
         \State Zamień liść l węzłem z testem na atrybucie $X_a$
         \State Dodaj pusty liść dla każdej gałęzi testu
      \EndIf
    \EndIf
  \EndIf
\EndFor
\EndFunction
\end{algorithmic}
\end{algorithm}

Jeżeli dwa lub więcej atrybutów przez długi czas będą miały bardzo zbliżone wartości \emph{zysku informacyjnego}, to nierówność Hoeffdinga nie pozwoli na wybranie lepszego z nich. Aby poradzić sobie z problemem remisu, drzewo w procesie konstrukcji wykorzystuje stały parametr: $\tau > 0$. W momencie, w którym zachodzi warunek $\epsilon < \tau$, do podziału zachodzi niezależnie od różnicy dla \emph{zysków informacyjnych}. Ponieważ wraz ze wzrostem $n$, wartość $\epsilon$ dąży do zera, to po określonej liczbie obserwacji w liściu musi dojść do rozwiązania remisu.

Gama w swoich publikacjach opisuje dodatkowe modyfikacja algorytmu: wykorzystanie do klasyfikacji w liściach naiwnego klasyfikatora bayesowskiego czy użycie drzew dla strumieni ewolucyjnych \cite{gama1}, \cite{gama2}.


\subsection{Dowód podobieństwa do drzewa C4.5}

Bardzo istotną właściwością drzew Hoeffdinga jest możliwość zagwarantowania, przy realistycznych założeniach, że drzewo stworzone w procesie nauki jest asymptotycznie podobne do drzewa wytworzonego przez algorytm biorący pod uwagę wszystkie dostępne dany określając test dla każdego wierzchołka (t.j. klasyczny klasyfikator C4.5).
Innymi słowy, strumieniowy charakter algorytmu nie powoduje znaczących różnic w skuteczności końcowego klasyfikatora.
Dokładne sformułowanie opracowane przez Domingosa i Hultena, zapisane jest w twierdzeniu \ref{tw:domingos} wraz z jego dowodem \cite{domingos}.

Niech $P(\vec{x})$ będzie prawdopodobieństwem zaobserwowania obserwacji $\vec{x}$, oraz niech $I(.)$ będzie funkcją charakterystyczną, zwracającą wartość $0$ dla fałszu i $1$ dla prawdy.

\begin{defi}[Zewnętrzna niezgoda, ang. \emph{Extensional disagreement}]
Zewnętrzną niezgodą $\Delta_e$ pomiędzy dwoma drzewami decyzyjnymi $T_1$ i $T_2$ nazywa się prawdopodobieństwo uzyskania z nich różnych predykcji dla losowej obserwacji:
$$\Delta_e(T_1,T_2) = \sum_xP(x)I[T_1(x) \neq T_2(x)]$$
\end{defi}

Niech dwa węzły drzewa będą uważane za różne jeśli zawierają inne testy, dwa liście jeśli dają sprzeczną predykcję, oraz węzeł z testem będzie różny od liścia. Dwie ścieżki w drzewie uznaje sie za różne jeśli różnią się dowolnym węzłem.

\begin{defi}[Wewnętrzna niezgoda, ang. \emph{Intensional disagreement}]
Wewnętrzną niezgodą $\Delta_i$ pomiędzy dwoma drzewami decyzyjnymi $T_1$ i $T_2$ nazywa się prawdopodobieństwo uzyskania różnych ścieżek w procesie predykcji dla losowej obserwacji:
$$\Delta_i(T_1,T_2) = \sum_xP(x)I[Path_1(x) \neq Path_2(x)]$$
\end{defi}

Należy zauważyć, że dwa drzewa są wewnętrznie zgodne dla obserwacji wtedy i tylko wtedy kiedy zachowują się dokładnie tak samo dla tej obserwacji: obserwacja jest zbadana przy pomocy dokładnie tych samych testów i ostatecznie uzyskuje tę samą klasyfikację. Wynika z tego, że wewnętrzna zgodność jest warunkiem trudniejszym do spełnienia i zachodzi następująca nierówność:
$$\bigforall_{T_1, T_2} \Delta_i(T_1,T_2) \geq \Delta_e(T_1,T_2)$$

Niech $p_l$ będzie prawdopodobieństwem, że obserwacja, która w drzewie decyzyjnym osiągnęła głębokość $l$ zostanie sklasyfikowana przez liść na tym poziomie. W celu uproszczenia zostanie przyjęte założenie, że to prawdopodobieństwo jest stałe, tzn. $\forall_lp_l=p$. Niech $p$ będzie dalej nazywane \emph{prawdopodobieństwem liścia}.
Założenie to jest realistyczne dla drzew decyzyjnych stworzonych w praktyce i w typowych przypadkach jest dobrą aproksymacją.

Niech $HT_\delta$ będzie drzewem stworzonym przez klasyfikator Hoeffdinga z ustalonym parametrem $\delta$ i przy użycia nieskończonego strumienia danych $\{(\vec{x_1},y_1),(\vec{x_2},y_2),\dots\}$. Niech $DT_*$ będzie drzewem decyzyjnym powstałym w skutek wykorzystania tej samej funkcji podziału, ale na pełnym zbiorze danych. Oznacza to, że podczas określania testu dla każdego węzła, znana była prawdziwa wartość tej funkcji na pełnym zbiorze nieskończonych danych. Dodatkowo poprzez $E[\Delta_i(HT_\delta, DT_*)]$ należy rozumieć wartość oczekiwaną wewnętrznej niezgody tych drzew powstałych na skutek wszystkich możliwych strumieni danych.

\begin{tw}
\label{tw:domingos}
Dla drzewa Hoeffdinga $HF_\delta$ i drzewa decyzyjnego utworzonego na podstawie pełnego zbioru danych $DT_*$, zachodzi zależność: $E[\Delta_i(HT_\delta, DT_*)] \leq \delta/p$.
Gdzie $p$ to wartość \emph{prawdopodobieństwa liścia}.
\end{tw}

\begin{proof}
Obserwacja $\vec{x}$ dla $HT_\delta$ zostaje sklasyfikowana poprzez liść $l_h$, a przez $DT_*$ poprzez liść $l_d$. Niech $l = min(l_h,l_d)$, a $Path_H(\vec{x})=(N_1^H(\vec{x}),N_2^H(\vec{x}),\dots,N_l^H(\vec{x}))$ będzie ścieżką dla $\vec{x}$ w drzewie $HT_\delta$ aż do poziomu $l$, tzn. że $N_i^H(\vec{x})$ jest wierzchołkiem drzewa na głębokości $i$ przez który przechodzi obserwacja.
Analogiczne rozumowanie zachodzi dla $Path_D(\vec{x})$ jako o ścieżce do poziomu $l$ w drzewie $DT_*$. Jeśli $l_h = min(l_h,l_d)$ to $N_l^H(\vec{x})$ jest liściem, który klasyfikuje obserwację, analogiczna zależność oczywiście zachodzi również dla drugiego drzewa.
$I_i$ zdefiniowano jako następujący predykat: \emph{``$Path_H(\vec{x}) = Path_D(\vec{x})$ do głębokości $i$, włącznie.''}. Należy zauważyć, że predykat $I_0$ jest zawsze prawdziwy. Zachodzi następująca zależność:
$$ P(Path_H(\vec{x}) \neq Path_D(\vec{x})) = P(N_1^H(\vec{x}) \neq N_1^D(\vec{x}) \lor \dots \lor N_l^H(\vec{x}) \neq N_l^D(\vec{x}))$$
W celu zwiększenia czytelności dalej przyjęto $N_i^H(\vec{x}) = N_i^H$ i $N_i^D(\vec{x}) = N_i^D$.
$$ P(Path_H(\vec{x}) \neq Path_D(\vec{x})) = P(N_1^H \neq N_1^D \lor \dots \lor N_l^H \neq N_l^D)$$
$$ P(Path_H(\vec{x}) \neq Path_D(\vec{x})) = P(N_1^H \neq N_1^D | I_0) + \ldots + P(N_l^H \neq N_l^D | I_{l-1})$$
\begin{equation}
\label{eq:ograniczenie}
  P(Path_H(\vec{x}) \neq Path_D(\vec{x})) = \sum_{i=1}^lP(N_i^H \neq N_i^D | I_{i-1}) \leq \sum_{i=1}^l\delta=l\delta
\end{equation}

Niech $HT_\delta(S)$ będzie drzewem Hoeffdinga wygenerowanym przez strumień danych $S$. Wtedy $E[\Delta_i(HT_\delta, DT_*)]$ jest średnią spośród wszystkich strumieni danych $S$ prawdopodobieństwa, że ścieżka w drzewie $HT_\delta(S)$ będzie różna od ścieżki w $DT_*$. Drzewo $DT_*$ nie zależy od kolejności $S$, ponieważ w procesie jego uczenia całość danych jest dostępna. Niech $L_i$ będzie zbiorem obserwacji, które są klasyfikowane przez dowolny liść na głębokości $i$.
$$E[\Delta_i(HT_\delta, DT_*)] = \sum_SP(S)\sum_{\vec{x}}P(\vec{x})I[Path_H(\vec{x}) \neq Path_D(\vec{x})]$$
$$E[\Delta_i(HT_\delta, DT_*)] = \sum_{\vec{x}}P(\vec{x})P(Path_H(\vec{x}) \neq Path_D(\vec{x}))$$
$$E[\Delta_i(HT_\delta, DT_*)] = \sum_{i=1}^{\infty}\sum_{\vec{x} \in L_i}P(\vec{x})P(Path_H(\vec{x}) \neq Path_D(\vec{x}))$$

Korzystając z nierówności \ref{eq:ograniczenie}:
$$E[\Delta_i(HT_\delta, DT_*)] \leq \sum_{i=1}^{\infty}\sum_{\vec{x} \in L_i}P(\vec{x})(i\delta)$$
$$E[\Delta_i(HT_\delta, DT_*)] \leq \sum_{i=1}^{\infty}(i\delta)\sum_{\vec{x} \in L_i}P(\vec{x})$$

Wartość $\sum_{\vec{x} \in L_i}P(\vec{x})$ jest równa prawdopodobieństwu, że dowolna obserwacja zostanie sklasyfikowana na głębokości $i$ i wynosi $(1-p)^{i-1}p$, gdzie $p$ to \emph{prawdopodobieństwo liścia}. Wartość może być interpretowana jako prawdopodobieństwo odniesienia sukces w $i$-tej próbie, gdzie prawdopodobieństwo sukcesu w jednej próbie wynosi $p$.

W konsekwencji:
$$E[\Delta_i(HT_\delta, DT_*)] \leq \sum_{i=1}^{\infty}(i\delta)(1-p)^{i-1}p$$
$$E[\Delta_i(HT_\delta, DT_*)] \leq p\delta\sum_{i=1}^{\infty}i(1-p)^{i-1}$$
$$E[\Delta_i(HT_\delta, DT_*)] \leq p\delta\sum_{j=1}^{\infty}(\sum_{i=j}^{\infty}(1-p)^{i-1})$$
$$E[\Delta_i(HT_\delta, DT_*)] \leq p\delta\sum_{j=1}^{\infty}\frac{(1-p)^{j-1}}{p}
      =\delta\sum_{j=1}^{\infty}(1-p)^{j-1} = \frac{\delta}{p}$$

Co kończy dowód.
\end{proof}

\subsection{Atrybuty numeryczne}

Większość problemów praktycznych jest opisywana za pomocą zmiennych o wartościach numerycznych. W celu zastosowaniu wyników teoretycznych w tych przypadkach konieczne jest uogólnienie opracowanych metod, tak aby mogły akceptować atrybuty tego typu.

W tej sekcji zostaną omówione metody budowania drzew Hoeffdinga z wykorzystaniem strumieni danych zawierających atrybuty numeryczne. Przedstawione zostaną dwie metody wykorzystywane przez badaczy we wcześniejszych pracach naukowych. Pierwsza, niestety niespełniająca wymagań stawianych przed algorytmami strumieniowymi, gromadzi duże ilości danych w celu wybierania bardzo dokładnych testów. Druga z nich optymalizuje wymagania pamięciowe, przyjmując mocne założenia i pogarszając jakość wybieranego testu.

\subsubsection{Wyczerupujące drzewo binarne (ang. \emph{Exhaustive binary tree})}

Zostało zaproponowane przez Gama et al. w systemie \emph{VFDTc} \cite{gama3}. W tym wypadku warunek testowy jest wybierany na podstawie wszystkich zaobserwowanych wartości. W szczególności wszystkie zaobserwowane wartości przetrzymywane są w drzewie binarnym, więc umieszczenie nowej wartości w strukturze danych ma złożoność $O(log~n)$, gdzie $n$ to liczba różnych wartości dotychczas zaobserwowanych. Każda unikalna wartość wiąże się też z nowym, stałym kosztem pamięciowym. W celu wyboru testu analizowane są wszystkie możliwe podziały, które można łatwo uzyskać na posortowanych w drzewie danych. Ponieważ dla danych numerycznych zbiór różnych wartości jest nieograniczony ta metoda narusza wymagania pamięciowe i czasowe stawiane przed algorytmami działającymi na strumieniach danych.

\subsubsection{Aproksymacja rozkładem Gaussa}

Szybka i wydajna pamięciowo metoda została zaproponowana przez tych samych autorów w kolejnej pracy \cite{gama4}. Metoda ta bazuje na analizie dyskryminacyjnej. Metoda zawdzięcza swoją wydajność potrzebie utrzymania tylko dwóch wartości dla każdego klasy względem atrybutu numerycznego: średniej i wariancji. Obie statystyki jak było pokazane w sekcji \ref{sec:stats} mogą być aktualizowane w czasie stałym.

W rozwiązaniu zaproponowanym w oryginalnej pracy metoda została opisana jako ograniczona do klasyfikacji dwóch klas. Autorzy proponują rozwiązanie problemu klasyfikacji dla $k$ różnych klas poprzez dekompozycję do $k(k-1)/2$ problemów dla dwóch klas i utrzymywanie lasu drzew. Jak słusznie zostało zauważone przez twórców platformy programistycznej \emph{MOA} takie podejście nie jest konieczne \cite{moa}.

W pierwszym zaproponowanym rozwiązaniu warunek testowy wybierany jest jako jeden z dwóch punktów przecięcia estymacji rozkładów klas. Możliwe jest uogólnienie tej metody, pozwalające na równoczesną analizę wielu klas. Zbiór możliwych punktów testowych, które należy rozważyć, ustalany jest jako stała liczba punktów na osi, równomiernie rozłożonych pomiędzy minimalną i maksymalną zaobserwowaną wartościa. Oczywiście minimalna i maksymalna wartość na zbiorze całych danych może być łatwo utrzymana. Dla każdego z badanych punktów wartość \emph{zysku informacyjnego} może następnie zostać wyliczona na podstawie aproksymacji rozkładów.

Działanie metody zobrazowane jest na rysunkach\footnote{Ilustracje pochodzą z publikacji \cite{moa}} \ref{fig:sub1} i \ref{fig:sub2}. Na każdym z rysunków zostały umieszczone aproksymacje rozkładów normalnych, które można określić na podstawie gromadzonych statystyk. W rozważanym przykładzie parametr określający liczbę punktów dla których rozważany jest podział wynosi $10$. Dla każdego z rozważanych punktów $x_1,\dots,x_{10}$ na podstawie estymacji poszczególnych rozkładów można określić prawdopodobieństwo zaobserwowania wartości mniejszych: $P(X<x_i|C=c)$. Dzięki temu dla każdego z punktów można oszacować odpowiadający mu \emph{zysk informacyjny}.

\begin{figure}[ht]
  \centering

  \begin{subfigure}{\linewidth}
    \centering
    \includegraphics[scale=0.3]{img/attr_num_1.png}
    \caption{Przykład dla dwóch klas}
    \label{fig:sub1}
  \end{subfigure}%

  \begin{subfigure}{\linewidth}
    \centering
    \includegraphics[scale=0.3]{img/attr_num_2.png}
    \caption{Przykład dla trzech klas}
    \label{fig:sub2}
  \end{subfigure}

  \caption{Drzewo Hoeffdinga może wykorzystać aproksymacje Gaussa do obsługi atrybutów numerycznych. Aproksymacje wartości dla takiego atrybutu tworzone są dla każdej z klas przy użyciu odpowiednich statystyk. Algorytm wylicza estymację \emph{zysku informacyjnego} wykorzystując aproksymację rozkładu. \emph{Zysk informatyczny} jest szacowany w stałej liczbie punktów kontrolnych i na podstawie tych estymacji algorytm podejmuje dalsze decyzje.}
  \label{fig:orientacja}
\end{figure}

Metoda dokonuje aproksymacji rozkładu każdej z klas względem atrybutu używając rozkładu normalnego. Jest to silne założenie, ponieważ rozkład normalny nie pozwoli na uchwycenie zawiłych rozkładów numerycznych, mimo to metoda często sprawdza się bardzo dobrze w praktyce. Należy również zauważyć, że przyjęte uproszczenie ma niewielkie szanse wywarcia negatywnego wpływu na jakość klasyfikacji. Wraz z procesem uczenia, ewentualne błędy w podziale będą mogły być poprawione na niższych poziomach drzewa decyzyjnego.

\clearpage
