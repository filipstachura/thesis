var Hapi = require("hapi");
var socketio = require("socket.io");

var port = process.env.PORT || 8080;
var app = Hapi.createServer(port);

app.route({
  method: 'GET',
  path: '/{param*}',
  handler: {
    directory: {
      path: 'public/',
      listing: true
    }
  }
});
app.route({
  method: 'GET',
  path: '/bower_components/{param*}',
  handler: {
    directory: {
      path: 'bower_components/',
      listing: true
    }
  }
});

var io = socketio.listen(app.listener);

function readSignal(data, callback) {
  io.emit(data.name, data);
  io.emit('*', data);
  if (callback) {
    callback(null, "ok");
  }
}

function hello(callback) {
  if (callback) {
    callback(Date.now());
  }
}

io.sockets.on('connection', function (socket) {
  console.log('connection');
  socket.on('readSignal', readSignal);
  socket.on('hello', hello);
});

app.start();
