var host = "/";
var socket = io(host);

signalReader = (function signalReaderCreator() {
  var signalValues = {};

  return {
    set: function set(signalName, value) {
      signalValues[signalName] = value;
    },
    get: function get(signalName) {
      return signalValues[signalName];  
    }
  };
}());

socket.on('connect', function(){
  console.log('connection established');
  socket.on('*', function(event) {
    signalReader.set(event.name, event.value); 
  });
  socket.on('disconnect', function(){});
});
