var gulp = require("gulp");

var config = require("./gulp/config.json");
var spawn = require("./gulp/spawn.js");
var exec = require("./gulp/exec.js");
var run = require("./gulp/run.js");
var tslint = require("./gulp/tslint.js");
var mocha = require("./gulp/mocha.js");
var clean = require("./gulp/clean.js");
var ts = require("./gulp/ts.js");

var src = config.src;
var bin = config.bin;
var tmp = config.tmp;
var typings = config.typings;

gulp.task("clean-bin", clean(bin));
gulp.task("clean-tmp", clean(tmp));
gulp.task("clean", ["clean-bin", "clean-tmp"]);

gulp.task("tmp-dir", exec("mkdir -p " + tmp, {tag: "[tmp-dir]"}));

gulp.task("test-units", ["ts", "tmp-dir"], mocha(bin + "test/units/**/*.js", {coverageOutput: tmp + "coverage-units"}));
gulp.task("run-units", run("test-units", process.exit));

gulp.task("nodeify", ts([src + "**/*.ts", typings + "**/*.d.ts"], bin));
gulp.task("tslint", tslint([src + "**/*.ts", "!" + src + "typings/**/*.ts"]));
gulp.task("default", run("clean", "nodeify"));
