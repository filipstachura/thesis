'use strict';

var gulp = require('gulp');
var mocha = require('gulp-mocha');
var exit = require('gulp-exit');
var cover = require('gulp-coverage');
var fs = require('fs');
var util = require('util');

function mochaTask(src, opts) {
    opts = opts || {};

    opts.timeout = opts.timeout || 2000;
    opts.coverageOutput = opts.coverageOutput || 'coverage-output';

    return function () {
        if (process.env.NODE_ENV === "development") {
          process.env.NODE_ENV = "test";
        }
        return gulp.src(src)
            .pipe(cover.instrument({
                pattern: ['bin/**/*.js', '!bin/test/**'],
                debugDirectory: 'debug'
            }))
            .pipe(mocha({
                reporter: 'spec',
                timeout: opts.timeout
            }))
            .on("error", function(err) {
              fs.writeFileSync("mocha.log", err + util.inspect(err));
              console.error("Testing failed. Error saved to mocha.log file.");
              process.exit(1);
            })
            .pipe(cover.report({
              outFile: opts.coverageOutput + '.html'
            }));
    };
};

module.exports = mochaTask;
