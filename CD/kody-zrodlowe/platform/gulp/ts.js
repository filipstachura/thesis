'use strict';

var gulp = require('gulp');
var typescript = require('gulp-tsc');

function tsTask(src, dest) {
    return function () {
        return gulp.src(src)
            .pipe(typescript({
                module: 'CommonJS',
                target: 'ES5',
                sourcemap: false
            }))
            .pipe(gulp.dest(dest));
    };
};

module.exports = tsTask;
