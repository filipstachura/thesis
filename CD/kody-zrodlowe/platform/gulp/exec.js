'use strict';

var gulp = require('gulp');
var gutil = require('gulp-util');
var exec = require('child_process').exec;

function execTask(cmd, params) {
    var tag = params && params.tag || '';

    return function () {
        gutil.log(tag, 'exec');

        exec(cmd, function (error, stdout, stderr) {
            gutil.log(tag, '' + stdout);
            gutil.log(tag, '' + stderr);

            if (error !== null) {
              gutil.log(tag, '' + error);
            }
        });
    };
};

module.exports = execTask;
