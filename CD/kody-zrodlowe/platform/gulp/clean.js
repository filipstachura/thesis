'use strict';

var gulp = require('gulp');
var rimraf = require('gulp-rimraf');

function cleanTask(src) {
    return function () {
        return gulp.src(src)
            .pipe(rimraf({force: true}));
    };
};

module.exports = cleanTask;
