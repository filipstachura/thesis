'use strict';

var gulp = require('gulp');
var gutil = require('gulp-util');
var spawn = require('child_process').spawn;
var _ = require('lodash');

function spawnTask(cmd, args, params) {
    params = params || {};
    var tag = params.tag || '';
    var callback = params.callback || function() {};
    params.env = params.env || {};

    var env = _.cloneDeep(process.env);
    env.NODE_ENV = params.env.NODE_ENV || env.NODE_ENV || "development";
    var options = { 
        stdio: [process.stdin],
        env: env
    };

    return function (finish) {
        var proc = spawn(cmd, args, options);

        gutil.log(tag, 'spawn');

        proc.stdout.on('data', function (data) {
            process.stdout.write(data);
        });

        proc.stderr.on('data', function (data) {
            gutil.beep();
            gutil.log(tag, gutil.colors.red('' + data));
        });

        proc.on('exit', function (code) {
            gutil.log(tag, 'exit (' + code + ')');
            callback(code);
            finish();
        });
    };
};

module.exports = spawnTask;
