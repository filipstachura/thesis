interface ITracer {
  console(options: any): any;
}

declare var tracer: ITracer;

declare module "tracer" {
    export = tracer;
}
