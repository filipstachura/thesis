declare module winston {
    export module transports {
        export class Console implements winston.IWinstonTransport {
            constructor(settings: winston.IWinstonTransportSettings);
        }
        export class File implements winston.IWinstonTransport {
            constructor(settings: winston.IWinstonTransportSettings);
        }
    }

    export class Logger {
        constructor(settings: IWinstonSettings);
        log(verbosityLevel: string, ...args: any[]): void;
        debug(...args: any[]): void;
        info(...args: any[]): void;
        warn(...args: any[]): void;
        error(...args: any[]): void;
    }

    export interface IWinstonSettings {
        transports: Array<IWinstonTransport>;
    }

    export interface IWinstonTransport { }

    export interface IWinstonTransportSettings {
        level?: string;
    }

    export function log(verbosityLevel: string, ...args: any[]): void;
    export function debug(...args: any[]): void;
    export function info(...args: any[]): void;
    export function warn(...args: any[]): void;
    export function error(...args: any[]): void;
    export function add(transport: IWinstonTransport, settings: IWinstonTransportSettings): void;
    export function remove(transport: IWinstonTransport): void;
}

declare module "winston" {
    export = winston;
}

