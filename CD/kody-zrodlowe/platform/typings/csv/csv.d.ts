interface ICSV {
  stringify(data: Array<any>, callback: Function): void;
  parse(data: string, options: any, callback: Function): void;
}

declare var csvModule: ICSV;

declare module "csv" {
    export = csvModule;
}
