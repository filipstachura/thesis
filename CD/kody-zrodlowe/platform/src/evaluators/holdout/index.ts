import fs = require('fs');
import path = require("path");
import csv = require('csv');
import yargs = require('yargs');
import client = require("socket.io-client");
import _ = require("lodash");

var argv = yargs.argv;
var socket;

var host = argv.host || "http://sensas-dispatcher.herokuapp.com/"; 
var windowSize = argv.windowsSize || 1000;
var csvPath = argv.csv;
var evaluationClass = argv.class;
var outputDir = argv.output;
var iteration = 0;
var header: Array<string>;
var rows;
var results;
var collectedResults;

function programExit(code) {
  if (socket) {
    socket.close();
  }
  process.exit(code);
}

function reportError(err) {
  console.log("Program error:", err);
  programExit(1);
}

process.on('SIGINT', function() {
  console.log("\nGracefully shutting down from SIGINT (Ctrl-C)");
  programExit(0);
})

if (!evaluationClass) {
  reportError("Evaluation class needs to be defined. Please specify via --class parameter.");
}
if (!csvPath) {
  reportError("CSV holdout data required to perform evaluation. Please specify via --csv parameter.");
}

function evaluateHoldout() {
  iteration = iteration + 1;
  results = {};
  collectedResults = 0;
  var id = 0;
  _.each(rows, function (row: Array<any>) {
    results[id] = undefined;
    var instance = _.zipObject(header, _.map(row, parseFloat));
    var signal = "evaluation-request-" + evaluationClass;
    socket.emit("readSignal", {
      name: signal, 
      value: instance,
      time: Date.now(), 
      id: id
    });
    id = id + 1;
  });
}

function saveEvaluationResults() {
  _.each(results, function (result: any, id: string) {
    rows[id].push(result.value);
  });
  var outputPath = path.join(outputDir, iteration + ".csv");
  csv.stringify(rows, function(err, data) {
    console.log("saving to ", outputPath, " ...");
    fs.writeFileSync(outputPath, data);
  });
}

function sumUpResults() {
  var correct = 0;
  var classIndex = _.indexOf(header, evaluationClass);
  _.each(results, function (result: any, id: string) {
    if (result.value === parseInt(rows[id][classIndex], 10)) {
      correct = correct + 1;
    }
  });

  var result = correct / rows.length;
  socket.emit("readSignal", {
    name: "evaluation-result-" + evaluationClass, 
    value: result,
    time: Date.now()
  });
  console.log("Holdout evaluation: ", result);

  if (outputDir) {
    saveEvaluationResults();
  }

  setTimeout(evaluateHoldout, windowSize);
}

function evaluationResult(result) {
  results[result.id] = result;
  collectedResults = collectedResults + 1;
  if (collectedResults === rows.length) {
    sumUpResults();
  }
}

var options = {
  trim: true,
};
csv.parse(fs.readFileSync(csvPath, 'utf8'), options, function (err, data) {
  header = data.splice(0, 1)[0];
  rows = data; 
  console.log("Connecting with socket.io:  ", host);
  socket = client.connect(host);
  socket.on('connect', function () {
    console.log("Connected with socket.io: ", host);
    setTimeout(evaluateHoldout, windowSize);
  });
  socket.on("evaluation-prediction-" + evaluationClass, evaluationResult);
});

