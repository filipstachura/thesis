import client = require("socket.io-client");
import yargs = require('yargs');

var argv = yargs.argv;

var host = argv.host || "http://sensas-dispatcher.herokuapp.com/"; 
var windowSize = argv.windowsSize || 1000;
var dataFromServer = {};
var className = argv.class;
var predictionName = "prediction-" + className;
var classified = 0;
var correctlyClassified = 0;

var socket = client.connect(host);

function freshDataReady() {
  if (!(dataFromServer[className] && dataFromServer[predictionName])) {
    return false;
  }
  var minTime = Math.min(dataFromServer[className].time, dataFromServer[predictionName].time);
  var currentTime = Date.now();
  var diff = Math.abs(currentTime - minTime);
  return diff < windowSize * 2;
}

function evaluate() {
  if (!freshDataReady()) {
    console.log("data not ready, skipping...");
    return;
  }

  if (dataFromServer[predictionName].value == dataFromServer[className].value) {
    correctlyClassified = correctlyClassified + 1;
  }
  classified = classified + 1;
  var acc = correctlyClassified / classified;
  console.log("acc", acc);
  socket.emit("readSignal", {
    name: "evaluation-prequential-" + className, 
    value: acc
  });
}

socket.on(className, function(signal) {
  dataFromServer[className] = signal;
});
socket.on(predictionName, function(signal) {
  dataFromServer[predictionName] = signal;
});

var interval = setInterval(evaluate, windowSize);

process.on('SIGINT', function() {
  console.log("\nGracefully shutting down from SIGINT (Ctrl-C)");
  socket.disconnect();
  clearInterval(interval);
  process.exit();
})
