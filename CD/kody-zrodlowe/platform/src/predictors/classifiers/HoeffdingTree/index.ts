import _ = require("lodash");
import path =require("path");
import java = require("java");
import Q = require("q");
import winston = require("winston");
import konfig = require("konfig");

import IClassifier = require("../IClassifier");

var MOA_CLASSPATH = path.join(__dirname, "../../../../jar/moa.jar");

// Initialize java classpath:
java.classpath.push("commons-lang3-3.1.jar");
java.classpath.push("commons-io.jar");
java.classpath.push(MOA_CLASSPATH);

// Import java classes:
var DenseInstance = java.import('weka.core.DenseInstance');
var JavaHoeffdingTree = java.import('moa.classifiers.trees.HoeffdingTree');
var Attribute = java.import('weka.core.Attribute');
var FastVector = java.import('weka.core.FastVector');
var Instances = java.import('weka.core.Instances');
var InstancesHeader = java.import('moa.core.InstancesHeader');

var config = konfig();
var logger = new (winston.Logger)({
  transports: [
    new (winston.transports.Console)({ level: config.main.logLevel })
  ]
});

class HoeffdingTree implements IClassifier {
  private tree;  
  private moaAttr: {[name: string]: any} = {};
  private ncols;
  private instances;

  constructor(public attributes: Array<string>, private klass: string, private klassCardinality: number, moaTree?: any) {
    logger.info("Creating classifier", attributes, klass);
    // TODO: should be an option
    if (typeof moaTree !== "undefined") {
      this.tree = moaTree;
    } else {
      this.tree = new JavaHoeffdingTree();  
    }
    this.tree.gracePeriodOption.setValueSync(10);
    this.ncols = this.attributes.length + 1; // +1 for class

    var attInfo = java.newInstanceSync("java.util.ArrayList");
    _.each(attributes, (attr: string): void => this.moaAttr[attr] = new Attribute(attr));
    var classNominal = new FastVector(this.klassCardinality);
    _.each(_.range(this.klassCardinality), (classIt: number): void => classNominal.addElementSync(classIt.toString()));
    this.moaAttr[klass] = new Attribute(klass, classNominal);
    _.each(this.moaAttr, (attr: any): void => attInfo.addSync(attr));
    this.instances = new Instances("testData", attInfo, 0);
    this.instances.setClassSync(this.moaAttr[klass]);
    this.tree.setModelContextSync(new InstancesHeader(this.instances));
    this.tree.prepareForUseSync();
    logger.debug("Created classifier with following context:", this.tree.getModelContextSync().toStringSync());
  }

  getAttributes(): Array<string> {
    return this.attributes;
  }

  getName(): string {
    return this.getAttributes().join("_");
  }

  duplicate(withAttributes: Array<string>): IClassifier {
    var treeCopy = this.tree.copySync();
    return new HoeffdingTree(withAttributes, this.klass, this.klassCardinality, treeCopy);;
  }

  learn(data: {[name: string]: any}): Q.Promise<void> {
    var trainInst = new DenseInstance(this.ncols);
    _.each(this.moaAttr, (attr: any, name: string): void => {
      if (typeof data[name] !== "undefined") {
        trainInst.setValueSync(attr, data[name]);
      }
    });
    trainInst.setDatasetSync(this.instances);
    return Q.npost(this.tree, "trainOnInstance", [trainInst]).thenResolve(null);
  }

  predict(data: {[name: string]: any}): Q.Promise<any> {
    var instance = new DenseInstance(this.ncols);
    _.each(this.attributes, (attr: string): void => {
      if (typeof data[attr] !== "undefined") {
        instance.setValueSync(this.moaAttr[attr], data[attr])
      }
    });
    instance.setDatasetSync(this.instances);
    this.instances.addSync(instance);
    logger.debug("getting votes for %s", instance.toStringSync());
    return Q.npost(this.tree, "getVotesForInstance", [instance])
      .then((votes: Array<number>): number => {
        if (votes.length == this.klassCardinality) {
          return votes.indexOf(Math.max.apply(Math, votes));
        }
        throw new Error("Classifier not ready. " + JSON.stringify(votes));
      })
      .fail((err: any): number => {
        logger.debug("Prediction error: %s", err.toString());
        return undefined;
      });
  }
}

export = HoeffdingTree;
