import Q = require("q");
import _ = require("lodash");
import client = require("socket.io-client");
import path = require("path");
import winston = require("winston");
import konfig = require("konfig");

import IPredictor = require("./IPredictor");
import IClassifier = require("./classifiers/IClassifier");
import ISensasEvent = require("../ISensasEvent");

var config = konfig();
var logger = new (winston.Logger)({
  transports: [
    new (winston.transports.Console)({ level: config.main.logLevel })
  ]
});

class Predictor implements IPredictor {
  private isLearning: boolean = false;
  protected klass: string;
  protected klassCardinality;
  protected classifierConstructor: new (attrs: Array<string>, klass: string, cardinality: number) => IClassifier;
  protected classifier: IClassifier;

  constructor(protected socket: client.Socket, classifierName: string, yaml: any) {
    this.klass = yaml.class.name;
    this.klassCardinality = parseInt(yaml.class.cardinality, 10);

    var classifierPath = path.join(__dirname, "classifiers", classifierName);
    this.classifierConstructor = require(classifierPath);
    this.classifier = new this.classifierConstructor(yaml.attributes, this.klass, this.klassCardinality);

    this.subscribe(yaml.subscribe);
  }

  listen(): Q.Promise<void> {
    var listening = Q.defer<void>();
    this.registerEvents();

    process.on("SIGINT", (): void => {
      logger.info("Predictor exiting...");
      this.socket.disconnect();
      this.shutdown()
        .then(listening.resolve);
    });
    return listening.promise;
  }

  protected shutdown(): Q.Promise<void> {
    return Q<void>(null);
  }

  private registerEvents(): void {
    this.socket.on("start-learn-" + this.klass, (): void => {
      this.isLearning = true;
      logger.info("State changed to: learning");
    });
    this.socket.on("stop-learn-" + this.klass, (): void => {
      this.isLearning = false;
      logger.info("State changed to: not learning");
    });
    this.socket.on("evaluation-request-" + this.klass, (testData): void => {
      this.classifier.predict(testData.value)
        .then((predicted: number): void => {
          this.socket.emit("readSignal", {
            name: "evaluation-prediction-" + this.klass,
            id: testData.id,
            value: predicted
          });
        });
    });
    logger.debug("Subscribed to predictor events.");
  }
  
  protected learnEvent(signal: ISensasEvent): Q.Promise<void> {
    return this.classifier.learn(signal.value);
  }

  protected postPredictionEvent(signal: ISensasEvent): Q.Promise<void> {
    return Q<void>(null);
  }

  private emitPrediction(predicted: number): void {
    logger.debug("Emit prediction %s", predicted);
    this.socket.emit("readSignal", {
      name: "prediction-" + this.klass,
      predictorStatus: this.isLearning,
      value: predicted
    });
  }

  protected predictionEvent(signal: ISensasEvent): Q.Promise<void> {
    var data = _.cloneDeep(signal.value);
    delete data[this.klass];

    return this.classifier.predict(data)
      .then(this.emitPrediction.bind(this))
      .then((): Q.Promise<void> => this.postPredictionEvent(signal));
  }

  private subscribe(name: string): void {
    this.socket.on(name, (signal): void => {
      this.predictionEvent(signal)
        .fail((err: any): void => logger.debug("Prediction event error: %s", err.toString()))
        .then((): Q.Promise<void> => {
          return this.isLearning ? this.learnEvent(signal) : Q<void>(null);
        })
        .fail((err: any): void => logger.debug("Learning event error: %s", err.toString()));
    });
  }

}

export = Predictor;
