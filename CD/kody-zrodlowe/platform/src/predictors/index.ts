import jsyaml = require("js-yaml");
import fs = require("fs");
import yargs = require("yargs");
import client = require("socket.io-client");
import Q = require("q");
import _ = require("lodash");
import assert = require("assert");
import winston = require("winston");
import konfig = require("konfig");

import IPredictor = require("./IPredictor");
import IClassifier = require("./classifiers/IClassifier");
import SelectivePredictor = require("./SelectivePredictor");

var config = konfig();
var logger = new (winston.Logger)({
  transports: [
    new (winston.transports.Console)({ level: config.main.logLevel })
  ]
});

var argv = yargs
  .usage("Starts sensas predictor.\nUsage: $0 -c <config> -l <classifier>")
  .require("c", "Config file required")
  .alias("c", "config")
  .describe("c", "Loads given YAML config")
  .default("h", "http://sensas-dispatcher.herokuapp.com/")
  .alias("h", "host")
  .describe("h", "Sensas dispatcher URL")
  .require("l", "Classifier class")
  .alias("l", "classifier")
  .argv;

var classifiers = ["HoeffdingTree"];
assert.ok(_.contains(classifiers, argv.classifier), "Selected classifier must be one of: " + JSON.stringify(classifiers));

Q(jsyaml.safeLoad(fs.readFileSync(argv.config, "utf8")))
  .then((yaml: any): Q.Promise<IPredictor> => {
    logger.info("Connecting with socket.io:", argv.host);
    var socket = client.connect(argv.host);
    var connected = Q.defer<IPredictor>();
    socket.on("connect", (): void => {
      logger.info("Connected.");
      connected.resolve(new SelectivePredictor(socket, argv.classifier, yaml));
    });
    socket.on("error", connected.reject);
    return connected.promise;
  })
  .then((predictor: IPredictor): Q.Promise<void> => {
    return predictor.listen();
  })
  .fail((err: any): void => {
    logger.info("Application error", err);  
  });

