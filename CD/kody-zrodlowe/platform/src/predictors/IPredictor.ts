import Q = require("q");

import ISensasEvent = require("../ISensasEvent");

interface IPredictor {
  listen(): Q.Promise<void>;
}

export = IPredictor;
