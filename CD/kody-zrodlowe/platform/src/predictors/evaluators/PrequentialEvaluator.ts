import IEvaluator = require("./IEvaluator");

class PrequentialEvaluator implements IEvaluator {

  private classified: number = 0;
  private correctlyClassified: number = 0;

  constructor(public name: string) { }

  report(predicted: number, expected: number): void {
    this.classified = this.classified + 1;
    if (predicted === expected) {
      this.correctlyClassified  = this.correctlyClassified + 1;
    }
  }

  status(): number {
    return this.correctlyClassified / this.classified; 
  }
}

export = PrequentialEvaluator;
