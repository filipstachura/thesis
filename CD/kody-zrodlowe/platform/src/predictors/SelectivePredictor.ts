import client = require("socket.io-client");
import _ = require("lodash");
import Q = require("q");
import winston = require("winston");
import konfig = require("konfig");
import path = require("path");
import fs = require("fs");

import Predictor = require("./Predictor");
import IClassifier = require("./classifiers/IClassifier");
import ISensasEvent = require("../ISensasEvent");
import IEvaluator = require("./evaluators/IEvaluator");
import PrequentialEvaluator = require("./evaluators/PrequentialEvaluator");

var config = konfig();
var logger = new (winston.Logger)({
  transports: [
    new (winston.transports.Console)({ level: config.main.logLevel })
  ]
});

class SelectivePredictor extends Predictor {

  private candidates: {[differAttr: string]: IClassifier} = {};
  private evaluators: {[differAttr: string]: IEvaluator} = {};
  private currentEvaluator: IEvaluator;
  private evaluationInterval;
  private spawnClassifier: (attributes: Array<string>) => IClassifier;

  private iteration_number = 0;
  private outputPath: string;

  private static GROW_THRESHOLD = 0.01;
  private static SHRINK_THRESHOLD = (-0.01);
  private static SELECTION_INTERVAL = 2 * 60 * 1000; // 2 minutes

  constructor(protected socket: client.Socket, classifierName: string, yaml: any) {
    super(socket, classifierName, yaml);
    
    this.spawnClassifier = (attributes: Array<string>): IClassifier => {
      return new this.classifierConstructor(attributes, this.klass, this.klassCardinality);
    };

    var timestamp = new Date().toISOString().replace(/T/, '_').replace(/\..+/, '');
    this.outputPath = path.join("./", "eval_" + this.klass + "_" + timestamp + ".csv");
    var header = "time,iteration,identifier,evaluation"; 
    fs.appendFile(this.outputPath, header + "\n", function (err) {
      if (err) { console.error("Write line error", err); }
    });

    this.classifier = this.spawnClassifier([]);
    this.currentEvaluator = new PrequentialEvaluator(this.classifier.getName());

    _.each(yaml.attributes, (attr: string): void => {
      this.candidates[attr] = null;
    });
    this.createCandidates();

    this.evaluationInterval = setInterval(this.evaluatorsReport.bind(this), SelectivePredictor.SELECTION_INTERVAL);
  }

  protected shutdown(): Q.Promise<void> {
    logger.info("SelectivePredictor exiting...");
    clearInterval(this.evaluationInterval);
    return Q<void>(null);
  }

  private createCandidates(): void {
    var attributeSet = this.classifier.getAttributes();
    _.each(_.keys(this.candidates), (attr: string): void => {
      var candidateAttrs = _.xor(attributeSet, [attr]);
      if (_.contains(attributeSet, attr)) {
        // Attribute removed. We have to start new classifier due to MOA "New context is not compatible with existing model" exception
        //this.candidates[attr] = this.spawnClassifier(candidateAttrs);
        delete this.candidates[attr];
        delete this.evaluators[attr];
      } else {
        this.candidates[attr] = this.classifier.duplicate(candidateAttrs); // produce a copy of the current one
        this.evaluators[attr] = new PrequentialEvaluator(this.candidates[attr].getName());
      }
    });
  }

  private switchToCandidate(name: string): void {
    this.classifier = this.candidates[name]; 
    this.currentEvaluator = new PrequentialEvaluator(this.classifier.getName());
    logger.info("Switching to:", this.classifier.getName());
    this.createCandidates();
  }

  private findBestIn(names: Array<string>): [string, number] {
    return _.reduce(names, (acc: [string, number], name: string): [string, number] => {
      var current = this.evaluators[name].status();
      logger.info("Candidate report:", name, current);
      if (typeof acc === "undefined" || current > acc[1]) {
        return [name, current];
      }
      return acc;
    }, undefined);
  }

  private evaluatorsReport(): void {
    var currentStatus = this.currentEvaluator.status();
    logger.info("Main report:", this.currentEvaluator.name, currentStatus);
    var bestCandidate = this.findBestIn(_.keys(this.candidates));
    if (typeof bestCandidate === "undefined") {
      return;
    }

    if (SelectivePredictor.GROW_THRESHOLD < bestCandidate[1] - currentStatus) {
      this.switchToCandidate(bestCandidate[0]); 
    } else {
      // Look for shorter model
      /*
      var shorterModels = _.filter(_.keys(this.candidates), (name: string): boolean =>
        this.candidates[name].getAttributes().length < this.classifier.getAttributes().length);
      bestCandidate = this.findBestIn(shorterModels);
      if (typeof bestCandidate === "undefined") {
        return;
      }
      if (SelectivePredictor.SHRINK_THRESHOLD < bestCandidate[1] - currentStatus) {
        this.switchToCandidate(bestCandidate[0]); 
      }
      */
    }
    this.iteration_number = this.iteration_number + 1;
  }

  private reportToFile(klassifier: IClassifier, evaluator: IEvaluator): void {
    var line = [Date.now(), this.iteration_number, klassifier.getName(), evaluator.status()].join(",");
    fs.appendFile(this.outputPath, line + "\n", function (err) {
      if (err) { console.error("Write line error", err); }
    });
  }

  private evaluateAndLearn(klassifier: IClassifier, evaluator: IEvaluator, signal: ISensasEvent): Q.Promise<void> {
    logger.debug("Evaluation %s %s", klassifier.getName(), JSON.stringify(signal));
    return klassifier.predict(signal.value)
      .then((prediction: number): void => {
        evaluator.report(prediction, parseInt(signal.value[this.klass], 10)); 
        this.reportToFile(klassifier, evaluator);
      })
      .fail((err: any): void => logger.debug("Prediction error: %s", err.toString()))
      .then((): Q.Promise<void> => klassifier.learn(signal.value))
      .fail((err: any): void => logger.debug("Learning error: %s", err.toString()))
  }

  protected learnEvent(zignal: ISensasEvent): Q.Promise<void> {
    var signal = _.cloneDeep(zignal);
    return this.evaluateAndLearn(this.classifier, this.currentEvaluator, signal)
      .fail((err: any): void => logger.debug("Learn event error: %s", err.toString()))
      .then((): Q.Promise<void> =>
        Q.all(_.map(this.candidates, (candidate: IClassifier, name: string): Q.Promise<void> =>
          this.evaluateAndLearn(candidate, this.evaluators[name], signal)
        )).thenResolve(null)
      )
      .fail((err: any): void => logger.debug("Learn event error: %s", err.toString()));
  }

  protected postPredictionEvent(signal: ISensasEvent): Q.Promise<void> {
    return Q.all(_.map(this.candidates, (candidate: IClassifier, attr: string): Q.Promise<number> => 
      candidate.predict(signal.value)
    ))
      .then((predictions: Array<number>): void => {
        //logger.info("Predictions: ", JSON.stringify(_.zipObject(_.keys(this.candidates), predictions))); 
      })
      .fail((err: any): void => logger.debug("Post prediction error: %s", err.toString()));
  }

}

export = SelectivePredictor;
