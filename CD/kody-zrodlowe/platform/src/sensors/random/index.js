import client = require("socket.io-client");
import yargs = require('yargs');

var argv = yargs.argv;

var host = argv.host || "http://sensas-dispatcher.herokuapp.com/";
var socket = client.connect(host);
var oneSecond = 1000;

var range = {
  start: argv.start || 0,
  end: argv.end || 100
};

var reportRandom = function reportRandom() {
  var value = Math.round(Math.random() * (range.end - range.start)) + range.start; 
  socket.emit("readSignal", {name: "random", value: value});
};

var interval = setInterval(reportRandom, oneSecond);

socket.on('connect', function () {
  console.log("Connected with host: ", host);
});

process.on( 'SIGINT', function() {
  console.log("\nGracefully shutting down from SIGINT (Ctrl-C)");

  socket.close();
  clearInterval(interval);
  process.exit();
})
