var config = {
  host: "http://sensas-dispatcher.herokuapp.com/",//"http://192.168.0.11:8080/",
  uuid: "B9407F30-F5F8-466E-AFF9-25556B57FE6D",
  major: "42448",
  serverTimeShift: undefined
};


var app = {
    initialize: function() {
      this.bindEvents();
    },
    // Bind Event Listeners
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
      document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function() {
      app.receivedEvent('deviceready');
      handleConnection();
    },
    receivedEvent: function(id) {
      console.log('Received Event: ' + id);
    }
};

$(document).ready(function() {
  $(".navbar li a").click(function() {
      var select = $(this).data("value");
      $(".navbar li.active").toggleClass("active");
      $(this).parent().toggleClass("active");
      $("#" + select + "-tab").siblings().hide();
      $("#" + select + "-tab").show();
      $('.navbar-collapse').collapse('toggle');
  });
});
