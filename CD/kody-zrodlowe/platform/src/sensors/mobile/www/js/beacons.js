var monitorBeacons = function(emitter, subscriber) {
  var minor = undefined;
  var major = config.major;
  var uuid = config.uuid;
  var identifier = "testIdentifier";

  var registeredValues = {};

  var delegate = new cordova.plugins.locationManager.Delegate().implement({
    didDetermineStateForRegion: function (result) {
      //console.log('[DOM] didDetermineStateForRegion: ' + JSON.stringify(pluginResult));
    },
    didStartMonitoringForRegion: function (result) {
      //console.log('didStartMonitoringForRegion:', pluginResult);
    },
    didRangeBeaconsInRegion: function (result) {
      var names = _.map(result.beacons, function(beacon) {
        var minor = beacon.minor;
        var name = "b" + minor;
        return name;
      });
      var rssis = _.map(result.beacons, function(beacon) {
        return beacon.rssi;
      });

      registeredValues = _.assign(registeredValues, _.zipObject(names, rssis));
      var value = registeredValues;
      var signals = _.keys(registeredValues);
      if (signals.length > 2) {
        var copy = signals[_.random(signals.length - 1)];
        value["b7"] = registeredValues[copy];
      }

      var selected = $("#selectedRoom .btn:first-child").val();
      if (selected) {
        value["room"] = selected;
      }
      emitter("readSignal", {
        name: "beacons", 
        value: value,
        time: Date.now() + config.serverTimeShift
      });
      registeredValues = {};
    }
  });

  var beaconRegion = new cordova.plugins.locationManager.BeaconRegion(identifier, uuid, major, minor);
  cordova.plugins.locationManager.setDelegate(delegate);
  cordova.plugins.locationManager.startRangingBeaconsInRegion(beaconRegion)
    .then(function () {
      console.log("started ranging", arguments);
    })
    .fail(console.error)
    .done();

  $("#selectedRoom .dropdown-menu li a").click(function(){
      $("#selectedRoom .btn:first-child").text($(this).text());
      $("#selectedRoom .btn:first-child").val($(this).data('value'));
  });
  function setLearning(val) {
    $("#beacons-tab .start-learning").prop("disabled", val);
    $("#beacons-tab .stop-learning").prop("disabled", !val);
  }
  $("#beacons-tab .start-learning").click(function () {
    emitter("readSignal", {
      name: "start-learn-room", 
      value: undefined,
      time: Date.now() 
    });
  });
  $("#beacons-tab .stop-learning").click(function () {
    emitter("readSignal", {
      name: "stop-learn-room", 
      value: undefined,
      time: Date.now() 
    });
  });
  
  subscriber("prediction-room", function(signal) {
    var prediction = signal.value + 1;
    $("#roomPrediction").val(prediction);
    setLearning(signal.predictorStatus);
  });
};
