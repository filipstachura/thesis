function statesDescription() {
  var states = {};
  states[Connection.UNKNOWN] = 'Unknown connection';
  states[Connection.ETHERNET] = 'Ethernet connection';
  states[Connection.WIFI] = 'WiFi connection';
  states[Connection.CELL_2G] = 'Cell 2G connection';
  states[Connection.CELL_3G] = 'Cell 3G connection';
  states[Connection.CELL_4G] = 'Cell 4G connection';
  states[Connection.NONE] = 'No network connection';
  return states;
}

function pleaseConnect(socket) {
  console.log("connecting to host", config.host);
  socket.connect();
}

function pleaseDisconnect(socket) {
  console.log("disconnecting");
  socket.disconnect();
}

function updateConnectionInfo(status, desc) {
  $("#connectionPanel").toggleClass("panel-default", !status);
  $("#connectionPanel").toggleClass("panel-danger", !status);
  $("#connectionPanel").toggleClass("panel-success", status);
  $("#connectionPanel .panel-body").text(desc);
}

function handleConnection() {
  var socket = io.connect(config.host);
  var lastDesc;
  var states = statesDescription();

  socket.on("error", function() {
    console.log("CONNECTION ERROR");
  });
  socket.on("connect", function() {
    console.log("connected");
    socket.emit("hello", function (result) {
      config.serverTimeShift = result - Date.now();
      console.log("shift", config.serverTimeShift);
    });
  });
  
  var emitter = function(signalName, data) {
    socket.emit(signalName, data);
  }
  var subscriber = function(signalName, handler) {
    console.log("subscribe", signalName);
    socket.on(signalName, handler);
  }

  monitorBeacons(emitter, subscriber);
  //monitorOrientation(emitter, subscriber);
  //initScratcher(emitter);

  function checkIfWIFI() {
    var networkState = navigator.network.connection.type;
    var isConnected = socket.connected;

    /*
    if (networkState === Connection.WIFI && isConnected === false) {
      pleaseConnect(socket);
    }
    if (networkState !== Connection.WIFI && isConnected === true) {
      pleaseDisconnect(socket);
    }
    */

    var desc = (isConnected?"connected":"not connected") + " [" + states[networkState] + "]";
    if (desc !== lastDesc) {
      lastDesc = desc;
      updateConnectionInfo(isConnected, desc);
    }
  } 

  setInterval(checkIfWIFI, 5000);
}

