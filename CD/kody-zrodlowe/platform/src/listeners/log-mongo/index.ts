var client = require("socket.io-client");
var argv = require('yargs').argv;
var _ = require("lodash");
var fs = require("fs");
var MongoClient = require('mongodb').MongoClient;
var Q = require("q");

var host = argv.host || "http://sensas-dispatcher.herokuapp.com/";
var dbHost = argv.mogno || "mongodb://localhost:27017/sensas";
var socket;
var db;
var configPath = argv.config;

if (!configPath) {
  console.log("Config file required. Please specify via --config parameter.");
  process.exit();
}

var acceptSignals = fs.readFileSync(configPath).toString().split("\n");

function reportSignal(signal) {
  if (_.contains(acceptSignals, signal.name)) {
    var signals = db.collection('signals');
    Q.npost(signals, "insert", [signal])
      .fail(function(err) {
        console.log("insert error: ", err);
      });
  }
}

function connected(dbConnection) {
  console.log("Connected with mongo: ", dbHost);
  db = dbConnection;

  console.log("Connecting with socket.io:  ", host);
  socket = client.connect(host);
  socket.on('connect', function () {
    console.log("Connected with socket.io: ", host);
  });

  socket.on('*', reportSignal);
}

function connectionFailed(err) {
  console.log("Not connected to mongo", err);
  process.exit();
}

function shutdown() {
  console.log("\nGracefully shutting down from SIGINT (Ctrl-C)");

  if (socket) {
    socket.close();
  }
  if (db) {
    db.close();
  }
  process.exit();
}

console.log("Connecting with mongo: ", dbHost);
Q.npost(MongoClient, "connect", [dbHost])
  .then(connected, connectionFailed);

process.on('SIGINT', shutdown);
