import client = require("socket.io-client");
import tracer = require('tracer');
import yargs = require('yargs');

var argv = yargs.argv;

var logger = tracer.console({
  format: "{{timestamp}} {{message}}",
  dateformat: "HH:MM:ss",
});

var host = argv.host || "http://sensas-dispatcher.herokuapp.com/";
var socket = client.connect(host);

socket.on('connect', function () {
  console.log("Connected with host: ", host);
});

socket.on('*', function(signal) {
  logger.log('[%s] %s', signal.name, JSON.stringify(signal.value));
});

process.on('SIGINT', function() {
  console.log("\nGracefully shutting down from SIGINT (Ctrl-C)");

  socket.disconnect();
  process.exit();
})
